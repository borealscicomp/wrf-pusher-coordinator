#!/usr/bin/env python

import datetime
import json
import logging
import os
import subprocess
import sys
import uuid







LOGGER = logging.getLogger('Pusher')
handler = logging.StreamHandler()
formatter = logging.Formatter(
                '%(levelname)-8s PUSHER: %(funcName)s:%(lineno)d--%(message)s')
handler.setFormatter(formatter)
LOGGER.addHandler(handler)
LOGGER.setLevel(logging.DEBUG)



DEFAULT_MSG_SCRATCH_DIR='/tmp/pusher_msg_scratch'



class Pusher(object):

    def __init__(self, coordinator_path=None, 
                 ssh_command=None,
                 scp_command=None,
                 userhost=None,
                 local_msg_scratch_dir=None):

        """
        coordinator_path: Path to the coordinator executable on server
        ssh_command, scp_command: Full paths and args
        userhost: Full user@host.fqdn for server
        local_msg_scratch_dir: Where to store local message files
        """

        self._coordinator_path = coordinator_path
        self._ssh_command = ssh_command
        self._scp_command = scp_command
        self._userhost = userhost
        
        # Make sure the msg scratch directory exists
        if not os.path.isdir(local_msg_scratch_dir):
            try:
                os.makedirs(local_msg_scratch_dir, 0o755)

                #LOGGER.info('Created dir: %s' % incoming_requests_dir)
            except:  
                raise OSError('Failed to create: %s' % 
                                 local_msg_scratch_dir)
        
        self._local_msg_scratch_dir = local_msg_scratch_dir
        
        # Coordinator system info that will need to be filled in
        # before operations can proceed
        self._remote_request_dir = None
        self._stdout_response_delim = None

        self._coordinator_info_retrieved = False        

    def _coordinator_request_dir(self):
        return self._remote_request_dir
    
    def _coordinator_stdout_delim(self):
        return self._stdout_response_delim


    def _scpget(self, remote_path, local_path):
        
        success = None

        # Delete the local path if it exists, so we can check for new
        # file after retrieval
        if os.path.isfile(local_path):
            os.remove(local_path)
        
        # Fetch it
        the_command = self._scp_command.split()
        the_command += [self._userhost + ":" + remote_path, 
                        local_path]
        LOGGER.debug('the scpget command: ' + str(the_command))
        scpresult = subprocess.run(the_command,
                                   stdout=subprocess.DEVNULL,
                                   stderr=subprocess.DEVNULL)
        returncode = scpresult.returncode
        if returncode != 0:
            return False

        # Secondary check of success
        if os.path.isfile(local_path):
            LOGGER.debug('Got it: %s' % local_path)
            success = True
        else:
            LOGGER.debug('Did not get it: %s' % local_path) 
            success = False
    
        return success
    
    def _scpput(self, local_path, remote_path):
        
        success = None

        
        # Put it
        the_command = self._scp_command.split()
        the_command += [local_path, self._userhost + ":" + remote_path] 
        LOGGER.debug('the _scpput  command: ' + str(the_command))
        subprocess.call(the_command)
        scpresult = subprocess.run(the_command,
                                   stdout=subprocess.DEVNULL,
                                   stderr=subprocess.DEVNULL)
        returncode = scpresult.returncode

        if returncode == 0:
            success = True
        else:
            success = False

        return success


    

    def retrieve_coordinator_info(self):
        
        """
        Contacts coordinator with a simple sysinfo request, gets
        remote json filepath in response, fetches it, extracts JSON,
        assigns to this object
        
        This is somewhat involved because it's called before we know
        anything about the server, such as the stdout delimiter it will
        be using, where we need to put JSON requests, etc.
        
        If successful, sets global 

            self._coordinator_info_retrieved        
        
        to True and returns that value, otherwise, remains False and returns
        that value immediately
        """

        # Start off assuming False
        self._coordinator_info_retrieved = False

        # Create unique stdout filename for capturing the stdout of
        # the ssh'd event
        timestr = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S.%f')
        stdout_filename = 'stdout_' + timestr + '_' + str(uuid.uuid4()) + '.txt'
        stdout_filepath = self._local_msg_scratch_dir + '/' + stdout_filename
        LOGGER.debug('stdout_filepath: %s' % stdout_filepath)
        
        the_command = self._ssh_command.split() 
        the_command += [self._userhost, self._coordinator_path, 'sysinfo']
        LOGGER.debug('the_command: ' + str(the_command))
        
        # Execute the ssh call and capture stdout to local file
        try:
            ssh_stdout_fh = open(stdout_filepath, 'w')
            ssh_result = subprocess.run(the_command, stdout=ssh_stdout_fh)
            return_code = ssh_result.returncode
            if return_code != 0:
                LOGGER.error("ssh nonzero return code")
                return False
            ssh_stdout_fh.close()
        except:
            LOGGER.error("Failure in ssh request/response...")
            return False        

        # Now get the retrieved stdout and delete after reading
        try:
            fh = open(stdout_filepath, 'r')
            response_str = fh.read().strip()
            LOGGER.debug('response_str: ' + repr(response_str))
            fh.close
            os.remove(stdout_filepath)
        except:
            LOGGER.error("Failure reading stdout string")
            return False                    
        
        # Response json file path - we don't expect a delimiter, 
        # because this is our first contact, trying to figure out
        # what the delimiter is
        try:
            json_response_filepath = self._extract_delimited_substr(
                    s=response_str,
                    delimiter=None)
            LOGGER.debug('json_response_filepath: ' + repr(json_response_filepath))
        except:
            LOGGER.error("Failure extracting json response filepath...")
            return False
            
        if json_response_filepath:
            local_response_filepath = self._local_msg_scratch_dir + '/'
            local_response_filepath += os.path.basename(json_response_filepath)

    
            scpget_success = self._scpget(remote_path=json_response_filepath,
                                          local_path=local_response_filepath)
            if not scpget_success:
                LOGGER.error('Retrieval of json response filepath failed: ' +
                             repr(json_response_filepath))
                return False
            
            # extract the json
            try:
                fh = open(local_response_filepath, 'r')
                response_dict = json.load(fh)
                fh.close
            except:
                LOGGER.error('Extraction of JSON failed: ' + 
                             repr(local_response_filepath))
                return False
                
        else:
            LOGGER.error('json_response_filepath not extracted...')
            response_dict = None
            return False


        # We're almost there.  We've run the gauntlet.  Just check that
        # we can assign keys and assign values to globals
        try:
            self._remote_request_dir = response_dict['request_dir']
            self._stdout_response_delim = response_dict['stdout_delimiter']
        except:
            LOGGER.error('Failure accessing response dict keys...')
            return False
        
        LOGGER.debug('self._remote_request_dir: %s' % self._remote_request_dir)
        LOGGER.debug('self._stdout_response_delim: %s' % self._stdout_response_delim)


        self._coordinator_info_retrieved = True
        
        return self._coordinator_info_retrieved

    def run_remote_coord_command(self, coord_command=None, cli_args=None):

        """
        Uses ssh to run the coordinator followed by a command recognized
        by the coordinator, followed by a list of zero or more command line args 
        recognized by the command. 
        
        coord_command: The keyword command understood by the coordinator.
        Something like 'jsonrequest'
        
        cli_args: The "subcommand" args (including the subcommand itself),
        understood by the coord_command.
        
        Example:
            
            coord_command: jsonrequest
            cli_args: [<path-to-json-request-file>]
       
            In this example the json request file is assumed to already
            be on the server in the incoming request dir
            
        This function does a bit of back and forth, ultimately returning
        a dict that the remote command returns synchronously.  Note that
        in many cases, the remote command will continue executing, 
        producing an async reponse.  This method simply gets the sync response
        and, if there's something else to be gathered asynchronously, 
        fields in the sync response should be able to indicate to the
        caller of this method how to get it.
        
        Any failures will result in a return_dict of None being returned
        """

        # We need to make sure that certain coordinator info, like delimiter,
        # remote request dir, etc., has been obtained        
        if not self._coordinator_info_retrieved:
            self.retrieve_coordinator_info()
        
        
        # Create unique stdout filename for capturing the stdout of
        # the ssh'd event
        timestr = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S.%f')
        stdout_filename = 'stdout_' + timestr + '_' + str(uuid.uuid4()) + '.txt'
        stdout_filepath = self._local_msg_scratch_dir + '/' + stdout_filename
        LOGGER.debug('stdout_filepath: %s' % stdout_filepath)
        
        the_command = self._ssh_command.split() 
        the_command += [self._userhost, self._coordinator_path, coord_command]
        if cli_args:
            for arg in cli_args:
                the_command.append(arg)
        LOGGER.debug('the_command: ' + str(the_command))
        
        try:
            ssh_stdout_fh = open(stdout_filepath, 'w')
            ssh_result = subprocess.run(the_command, stdout=ssh_stdout_fh)
            return_code = ssh_result.returncode
            if return_code != 0:
                LOGGER.error("ssh nonzero return code")
                return False
            ssh_stdout_fh.close()
        except:
            LOGGER.error("Failure in ssh request/response...")
            return None        
        
        # Now get the retrieved stdout and delete after reading
        try:
            fh = open(stdout_filepath, 'r')
            response_str = fh.read()
            fh.close
            os.remove(stdout_filepath)
        except:
            LOGGER.error('Failure retrieving stdout...')
            return None
            
        
        # Response json file path - get it, extract from between delimiters
        try:
            json_response_filepath = self._extract_delimited_substr(
                    s=response_str,
                    delimiter=self._coordinator_stdout_delim())
        except:
            LOGGER.error('Failure extracting json_response_filepath...')
            return None
            


        LOGGER.debug('json_response_filepath: %s' % json_response_filepath)
        if json_response_filepath:
            local_response_filepath = self._local_msg_scratch_dir + '/'
            local_response_filepath += os.path.basename(json_response_filepath)



            scpget_success = self._scpget(remote_path=json_response_filepath,
                                          local_path=local_response_filepath)
            if not scpget_success:
                LOGGER.error('Failure to scp json_response_filepath: %s' % 
                             json_response_filepath)
                return None
            
  
            # extract the json
            try:
                fh = open(local_response_filepath, 'r')
                response_dict = json.load(fh)
                fh.close
            except:
                LOGGER.error('Failure extracting JSON: %s' %
                             local_response_filepath)
        else:
            LOGGER.error('json_response_filepath not extracted...')
            response_dict = None


        return response_dict        


    def json_request(self, command_str=None):


        """
        This method abstracts the notion of running remote command on
        the coordinator by doing the following:
            
            * Creating a properly formatted JSON file, locally, that the
            coordinator will know how to process
            * Transferring the JSON file to the server
            * Via ssh, call the coordinator program with the path to the
            server JSON file as an argument
            * Expect to receive a synchronous message back from the coordinator
            in the form of a dict, and return this to the caller.
            
        This method makes no attempt to interpret the synchronous dict 
        response.  In many cases, the command run by the coordinator will
        continue to run asynchronously, and information in the returned
        synchronous dict will help the caller determine how to interact with
        it over time.
        
        A couple of exceptions are checked for, and if they occur then we
        immediately log an error and return None for the response_dict.
        """
        
        
        # We need to make sure that certain coordinator info, like delimiter,
        # remote request dir, etc., has been obtained        
        if not self._coordinator_info_retrieved:
            self.retrieve_coordinator_info()
        
        # Create local JSON filepath
        timestr = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S.%f')
        jreq_filename = 'jsonrequest_' + timestr + '_' + str(uuid.uuid4()) + '.json'
        jreq_filepath = self._local_msg_scratch_dir + '/' + jreq_filename

        # Put the command in a properly formatted dict
        request_dict = {"task" : {"command_str" : command_str} } 
        LOGGER.debug('Pusher.json_request(): request_dict: %s' % request_dict)

        # convert and write the dict to JSON file        
        # Write the dict to the JSON file
        try:
            fh = open(jreq_filepath, 'w')
            json.dump(request_dict, fh)
            fh.close()
        except:
            LOGGER.error('Failure to dump JSON to: %s' % jreq_filepath)
            return None
        
        LOGGER.debug('Pusher.json_request(): Wrote JSON to file: %s' % jreq_filepath)

        
        # SCP the json file
        remote_filepath = self._coordinator_request_dir() + '/' + jreq_filename
        
        scpput_success = self._scpput(local_path=jreq_filepath,
                                      remote_path=remote_filepath)
        if not scpput_success:
            LOGGER.error('Failure to scp the json file to: %s' +
                         str(remote_filepath))
            return None
        
        # Remove local JSON file
        LOGGER.debug('Pusher.json_request(): removing jreq_filepath: %s' % jreq_filepath)
        os.remove(jreq_filepath)
        
        # Run coordinator via ssh command  
        # I won't try to cath exception here because the method has quite a bit
        # inside of it that should result in a None being returned, which this
        # method would then return
        LOGGER.debug('Pusher.json_request(): invoking run_remote_coord_command()')        
        response_dict = self.run_remote_coord_command(coord_command='jsonrequest', 
                                 cli_args=[remote_filepath])
        
        return response_dict



    def application_response(self, async_response_filepath=None):
        
        returned_dict = self.async_response(
                async_response_filepath=async_response_filepath)


        """
        2017-02-25
        
        This is what is currently expected to be returned from async_response()
        
        {
            'retrieve_success' : True/False
            'retrieved_localpath' : (just fyi)
            'message' : (fyi)
            'async_response_dict' : passed up to caller, not interpreted - this is what is de-jsonified.  It is None if the de-jsonification fails.
        }

        Then, async_response_dict is expected to be "application-compliant"        
        """
    
        try:
            appcomp_response_dict = returned_dict['async_response_dict']
        except:
            LOGGER.error('Unable to extract appcomp_response_dict')
            return None

    
        if appcomp_response_dict is None:
            LOGGER.debug('async_respone_file not retrieved...')
            return None
    
        # If we're here, then we got some kind of dict back.  Let's
        # see if it has everything it's suppose to have.  If not, return 
        # a MESG_ERROR status with copy of the received dict.  To be
        # compliant, the dict should have the four keys, but we make
        # no judgement here on what those values should be.  They can
        # all be None
        
        dict_is_application_compliant = True
        if type(appcomp_response_dict) is dict:
            if not 'status' in appcomp_response_dict.keys():
                LOGGER.debug('appcomp_response_dict does not have status key')
                dict_is_application_compliant = False
            if not 'utime_start' in appcomp_response_dict.keys():
                LOGGER.debug('appcomp_response_dict does not have utime_start key')
                dict_is_application_compliant = False
            if not 'utime_update' in appcomp_response_dict.keys():
                LOGGER.debug('appcomp_response_dict does not have utime_update key')
                dict_is_application_compliant = False
            if not 'app_specific_dict' in appcomp_response_dict.keys():
                LOGGER.debug('appcomp_response_dict does not have app_specific_dict key')
                dict_is_application_compliant = False
        else:
            LOGGER.debug('appcomp_response_dict is not a dict')
            dict_is_application_compliant = False


        if not dict_is_application_compliant:
            return_dict = {
                    'status' : 'MESG_ERROR',
                    'received_dict' : appcomp_response_dict
                    }
            return return_dict

        if dict_is_application_compliant:
            return appcomp_response_dict


    def async_response(self, async_response_filepath=None):
        
        """
        Attempts to probe for remote filepath and, if it can be retrieved,
        attempts to extract an async_response_dict from assumed JSON.
        
        We make no evaluation of the content.  We just try to extract the
        dict and return it to the caller
        """

        # Make sure coordinator info is loaded.  It "should" be if
        # we're trying to get a response, but might as well be safe
        if not self._coordinator_info_retrieved:
            self.retrieve_coordinator_info()

        
        # Set up the return dict structure
        return_dict = {
                'retrieve_success' : False,
                'retrieved_localpath' : None,
                'message' : None,
                'async_response_dict' : None
                }        
        
        # Try to scpget the async_response_filepath, then update
        # the return dict        
        local_response_filepath = self._local_msg_scratch_dir + '/'
        try:
            local_response_filepath += os.path.basename(async_response_filepath)
        except:
            LOGGER.error('Unable to create local_response_filepath')
            return_dict['message'] = 'Unable to create local_response_filepath for: '
            return_dict['message'] += str(async_response_filepath)
            return return_dict
        
        scpget_success = self._scpget(remote_path=async_response_filepath,
                                      local_path=local_response_filepath)
        if not scpget_success:
            LOGGER.error('Failure to scpget async_response_filepath: %s' % 
                         async_response_filepath)
            return_dict['message'] = 'Failed to scpget: %s' % async_response_filepath
            return return_dict
        else:
            return_dict['retrieve_success'] = True
            return_dict['retrieved_localpath'] = local_response_filepath

        
        # Try to extract async response dict and update return dict as needed
        try:
            fh = open(local_response_filepath, 'r')
            async_response_dict = json.load(fh)
            fh.close
        except:
            LOGGER.error('Failure extracting JSON: %s' %
                         local_response_filepath)
            return_dict['message'] = 'Failure extracting JSON: %s' % local_response_filepath
            return return_dict
        
        # Return the async_response_dict in the return_dict
        return_dict['async_response_dict'] = async_response_dict

        LOGGER.debug('return_dict: ' + str(return_dict))

        return return_dict



    def _extract_delimited_substr(self, s, delimiter=None):

        """
        Returns the string delimited on each end by delimiter
        
        If there are not exactly two instances of the delimiter, we
        assume something is wrong and return None
        
        If delimiter is None, we simply return s
        """        
        
        
        if delimiter is None or len(delimiter)==0:
            return s
        
        # Find first index of each delimiter occurence.
        delimiter_idx = []
        
        idx = 0
        while idx < len(s):
            idx = s.find(delimiter, idx)
            if idx == -1:
                break
            delimiter_idx.append(idx)
            idx += len(delimiter)
            
        if len(delimiter_idx) != 2:
            print('Need exactly two delimiters...')
            return None
        
        # If we're here, let's try to extract
        idx1 = delimiter_idx[0] + len(delimiter)
        idx2 = delimiter_idx[1] - 1
            
            
        return s[idx1:idx2+1]
    
            
        
        
        
        
        
        
        
        
        
        
        

if __name__=="__main__":
    
    
    COORDINATOR_PATH = '/home/morton/git/ssh-push-coord/'
    COORDINATOR_PATH += 'Coordinator/coordinator'
    
    # alaskawx
    SSH_COMMAND = '/usr/bin/ssh -p 51459'
    SCP_COMMAND = '/usr/bin/scp -P 51459'

    # polaris
    SSH_COMMAND = '/usr/bin/ssh'
    SCP_COMMAND = '/usr/bin/scp'

    COORDINATOR_USER_AND_HOST = "localhost"  # often user@hostname.fqdn 
    LOCAL_MSG_SCRATCH_DIR = '/tmp'


    P = Pusher(coordinator_path=COORDINATOR_PATH,
               ssh_command=SSH_COMMAND,
               scp_command=SCP_COMMAND,
               userhost=COORDINATOR_USER_AND_HOST,
               local_msg_scratch_dir=LOCAL_MSG_SCRATCH_DIR)


    
    LOGGER.debug('Pusher: P instantiated...')

    rpath = '/tmp/blahblah.txt'
    lpath = '/tmp/bluhbluh.txt'
    LOGGER.debug('_scpget() test...')
    success = P._scpget(rpath, lpath)
    print('success: %s' % success)
    
    LOGGER.debug('retrieve_coordinator_info() test...')    
    success = P.retrieve_coordinator_info()
    print('success: %s' % success)    
    

    """    
    s = 'ab$|$this is it$|$yz'
    delimiter = '$|$'
    s = 'ab' + delimiter + 'this is it' + delimiter + 'yz' + delimiter
    #s = 'this is it' + delimiter + 'yz'
    print('s: ' + str(s))
    t = P.extract_delimited_substr(s, delimiter)
    print('t: ' + str(t))
    """

    """    
    remote_command = '/home/morton/git/ssh-push-coord/'
    remote_command += 'Coordinator/test/functional/TestSleep.py'
    remote_command_str = remote_command + \
                    ' --method testsleep --iterations 3 --sleepsecs 10'
    
    remote_req_dir = P._coordinator_request_dir()    
    stdout_delim = P._coordinator_stdout_delim()

    LOGGER.debug('Pusher: remote_req_dir: %s' % remote_req_dir)
    LOGGER.debug('Pusher: stdout_delim: %s' % stdout_delim)

    LOGGER.debug('Pusher: OK, here goes...')
    

    # Run the command on the server.  We expect a synchronous response,
    # even if the command continues to run asynchronously
    response_dict = P.json_request(command_str=remote_command_str)    
    
    LOGGER.debug('sync response_dict: %s' % response_dict)
    
    # In the response dict we expect (for this command) an async_response_filepath
    async_response_filepath = response_dict['async_response_filepath']
    LOGGER.debug('async_response_filepath: %s' % async_response_filepath)
    
    # At this point we would expect (maybe polling, maybe after delaying) to
    # scp the async response file from server and extract its JSON to a dict
    """    
    
    
    
    
    
