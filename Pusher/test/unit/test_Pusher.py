#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 11 20:05:39 2019

@author: morton
"""

import unittest.mock
import unittest

import json
import shutil
import uuid

import Pusher


MSG_SCRATCH_DIR = '/tmp/' + str(uuid.uuid4())
SCP_COMMAND = '/usr/bin/scp -i /home/morton/.AWSKeys/GenericTesting.pem'
SSH_COMMAND = '/usr/bin/ssh -i /home/morton/.AWSKeys/GenericTesting.pem'

REMOTE_COORDINATOR_PATH = '/home/ubuntu/Coordinator/coordinator'
COORDINATOR_USER_AND_HOST = "ubuntu@50.112.179.250"

class Pusher_Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):

        cls.P = Pusher.Pusher(
                coordinator_path=REMOTE_COORDINATOR_PATH,
                ssh_command=SSH_COMMAND,
                scp_command=SCP_COMMAND,
                userhost=COORDINATOR_USER_AND_HOST,
                local_msg_scratch_dir=MSG_SCRATCH_DIR)        

    @classmethod
    def tearDownClass(cls):
        
        # Remove the directories
        shutil.rmtree(MSG_SCRATCH_DIR)


        
        
    def test_dummy(self):
        self.assertTrue(True)


    def test_pusher_raises_exception_for_bad_local_msg_scratch_dir(self):
        
        BAD_MSG_SCRATCH_DIR = '/ttt/baaadddd'
        
        self.assertRaises(OSError, Pusher.Pusher,
                coordinator_path=REMOTE_COORDINATOR_PATH,
                ssh_command=SSH_COMMAND,
                scp_command=SCP_COMMAND,
                userhost=COORDINATOR_USER_AND_HOST,
                local_msg_scratch_dir=BAD_MSG_SCRATCH_DIR)    
                         

    def test_extract_delimited_substr_success(self):
        
        """
        Tests that substring can be extracted in various formats
        """
        
        SUBSTRING1 = 'xxx&&&this is it&&&blah blah'
        result = self.P._extract_delimited_substr(SUBSTRING1, '&&&')
        self.assertEqual(result, 'this is it')
        
        SUBSTRING2 = '&&&this is it&&&'
        result = self.P._extract_delimited_substr(SUBSTRING2, '&&&')
        self.assertEqual(result, 'this is it')        

        SUBSTRING3 = '&&&this is it&&&blah blah'
        result = self.P._extract_delimited_substr(SUBSTRING3, '&&&')
        self.assertEqual(result, 'this is it')

        SUBSTRING4 = 'xxx&&&this is it&&&'
        result = self.P._extract_delimited_substr(SUBSTRING4, '&&&')
        self.assertEqual(result, 'this is it')

        SUBSTRING5 = 'this is it'
        result = self.P._extract_delimited_substr(SUBSTRING5)
        self.assertEqual(result, 'this is it')


    def test_extract_delimited_substr_failure_returns_none(self):
        
        """
        Tests that failed substring extraction returns None under
        various conditions
        """
        
        SUBSTRING1 = 'xxx&&&this is it&&'
        result = self.P._extract_delimited_substr(SUBSTRING1, '&&&')
        self.assertIsNone(result)
        
        SUBSTRING2 = 'xxx&&&this is it&&&and more&&&'
        result = self.P._extract_delimited_substr(SUBSTRING2, '&&&')
        self.assertIsNone(result)

        SUBSTRING3 = 'this is it&&&and more'
        result = self.P._extract_delimited_substr(SUBSTRING3, '&&&')
        self.assertIsNone(result)

        SUBSTRING4 = 'this is it'
        result = self.P._extract_delimited_substr(SUBSTRING4, '&&&')
        self.assertIsNone(result)








        
