#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 11 20:05:39 2019

@author: morton
"""

import unittest.mock
import unittest

import filecmp
import json
import os
import shutil
import subprocess
import time
import uuid

import Pusher



# Most of these will need to be changed when run outside of my
# alaskawx localhost system
MSG_SCRATCH_DIR = '/tmp/' + str(uuid.uuid4())
#SCP_COMMAND = '/usr/bin/scp'
#SSH_COMMAND = '/usr/bin/ssh'
SCP_COMMAND = '/usr/bin/scp -i /home/morton/.AWSKeys/GenericTesting.pem'
SSH_COMMAND = '/usr/bin/ssh -i /home/morton/.AWSKeys/GenericTesting.pem'
USERHOST = 'ubuntu@52.40.170.110' # General form is user@host

REMOTE_COORDINATOR_PATH = '/home/ubuntu/'
REMOTE_COORDINATOR_PATH += 'Coordinator/coordinator'
COORDINATOR_USER_AND_HOST = "ubuntu@52.40.170.110"

REMOTE_TESTSLEEP_PATH = '/home/ubuntu/'
REMOTE_TESTSLEEP_PATH += 'Coordinator/test/functional/TestSleep.py' 

class FunctionalPusher_Test(unittest.TestCase):

    def setUp(self):

        self.P = Pusher.Pusher(
                coordinator_path=REMOTE_COORDINATOR_PATH,
                ssh_command=SSH_COMMAND,
                scp_command=SCP_COMMAND,
                userhost=COORDINATOR_USER_AND_HOST,
                local_msg_scratch_dir=MSG_SCRATCH_DIR)        

    @classmethod
    def tearDownClass(cls):
        
        # Remove the directories
        pass
        shutil.rmtree(MSG_SCRATCH_DIR)


        
        
    def test_dummy(self):
        self.assertTrue(True)




    def test_retrieve_coordinator_info(self):
        
        """
        First insures that the known server request dir and delimiter are
        None, then calls to get the info, then checks that they are not None
        """
                         
        current_delimiter = self.P._coordinator_stdout_delim()
        self.assertIsNone(current_delimiter)

        current_request_dir = self.P._coordinator_request_dir()
        self.assertIsNone(current_request_dir)
        
        retrieve_success = self.P.retrieve_coordinator_info()
        self.assertTrue(retrieve_success)

        current_delimiter = self.P._coordinator_stdout_delim()
        self.assertIsNotNone(current_delimiter)

        current_request_dir = self.P._coordinator_request_dir()
        self.assertIsNotNone(current_request_dir)
        
        
    def test_retrieve_coordinator_info_returns_false_for_bad_ssh_command(self):        

        
        
        """
        Test that we get a clean exit and return of False if the ssh command
        is bad.
        """
        
        BAD_SSH_COMMAND = '/this/is/bad/ssh'
        
        P = Pusher.Pusher(
                coordinator_path=REMOTE_COORDINATOR_PATH,
                ssh_command=BAD_SSH_COMMAND,
                scp_command=SCP_COMMAND,
                userhost=COORDINATOR_USER_AND_HOST,
                local_msg_scratch_dir=MSG_SCRATCH_DIR)                

        retrieve_success = P.retrieve_coordinator_info()
        self.assertFalse(retrieve_success)
        
        # Also make sure our global flag is set correctly
        self.assertFalse(P._coordinator_info_retrieved)



    def test_run_remote_coord_command_jsonrequest_sync_response_ok(self):
        
        """
        Create a JSON request file with a valid command to be run by
        coordinator
        
        Get the Coordinator request dir and scp the JSON request file there
        
        Call the run_remote_coord_command() method, and expect a sync
        response in the form of a dict, which should contain status of OK,
        and an async JSON request file path

        """


        # Create JSON request file
        local_json_request_filepath = '/tmp/' + str(uuid.uuid4()) + '.json'
        remote_command = REMOTE_TESTSLEEP_PATH + ' --method testsleep '
        remote_command += ' --iterations 2 --sleepsecs 1'
        request_dict = {'task' : {
                'command_str' : remote_command
                }
            }
        with open(local_json_request_filepath, 'w') as fh:
            json.dump(request_dict, fh)

        # Get server incoming request dir, then scp to server
        self.P.retrieve_coordinator_info()
        coord_request_dir = self.P._coordinator_request_dir()
        coord_json_request_filepath = coord_request_dir + \
                os.path.basename(local_json_request_filepath)
        the_command = SCP_COMMAND.split()
        the_command += [local_json_request_filepath, 
                        USERHOST + ':' + coord_json_request_filepath]
        subprocess.call(the_command)
        
        # Remove the local file
        os.remove(local_json_request_filepath)


 
        # Now, set up the coordinator command
        remote_coord_command = 'jsonrequest'
        request_cliargs = [coord_json_request_filepath]

        sync_response_dict = self.P.run_remote_coord_command(
                coord_command=remote_coord_command,
                cli_args=request_cliargs)

        # Test that status is OK and that the async response filepath
        # is accessible and has "something" in it        
        self.assertEqual(sync_response_dict['status'], 'OK')
        self.assertTrue(len(sync_response_dict['async_response_filepath']) > 1)
    



    def test_json_request_jsonrequest_sync_response_ok(self):
        
        """
        Tests that the json_request() method will do its thang and
        return a valid sync response
        
        Create a command that the coordinator will be able to run with the
        "jsonrequest" option
        
        Submit the command via the json_request() method, and insure that
        we get back a response_dict that has a status of OK and an
        async_response_filepath string with length greater than one
        """
        
        remote_command_str = REMOTE_TESTSLEEP_PATH + ' --method testsleep '
        remote_command_str += ' --iterations 2 --sleepsecs 1'
        
        response_dict = self.P.json_request(command_str=remote_command_str) 
     
        # Test that status is OK and that the async response filepath
        # is accessible and has "something" in it        
        self.assertEqual(response_dict['status'], 'OK')
        self.assertTrue(len(response_dict['async_response_filepath']) > 1)    


    def test_application_response_returns_none_if_async_response_filepath_not_retrievable(self):
        
        self.assertIsNone(self.P.application_response(
                async_response_filepath='/bad/path/will/never/exist.json'))


    def test_application_response_returns_correct_stuff_if_async_json_response_correct(self):
        
        """
        Create a correct async_json_response file, put it on server, then
        retrieve and verify that what gets returned by application_response()
        is what we expect
        """

        pass

        # Create a simple test async response JSON file and upload it
        # to the server
        # Create JSON request file
        local_test_async_filepath = '/tmp/asynctest_' + str(uuid.uuid4()) + '.json'
        test_async_response_dict = {
                'status' : 'RUNNING',
                'utime_start' : 1551116954,
                'utime_update' : 1551116960,
                'app_specific_dict' : {'field1' : 1, 'field2' : True}
                }
        
        with open(local_test_async_filepath, 'w') as fh:
            json.dump(test_async_response_dict, fh)

        # Will just put the file in /tmp of the server
        # I can't give it exactly the same name though, because
        # if the client and server are on same machine then I'll have
        # a conflict
        remote_test_async_filepath = local_test_async_filepath + '.remote'
        
        success = self.P._scpput(local_path=local_test_async_filepath, 
                                 remote_path=remote_test_async_filepath)        
        self.assertTrue(success)   # Not really part of this test, but, wth        
        
        # Remove the local copy
        os.remove(local_test_async_filepath)

        appcomp_returned_dict = self.P.application_response(
                async_response_filepath=remote_test_async_filepath)
        
        # appcomp_returned_dict should be "application-compliant"        
        self.assertEqual(appcomp_returned_dict['status'], 'RUNNING')
        self.assertEqual(appcomp_returned_dict['utime_start'], 1551116954)
        self.assertEqual(appcomp_returned_dict['utime_update'], 1551116960)
        
        # Check that application specific dict has correct stuff
        self.assertEqual(appcomp_returned_dict['app_specific_dict']['field1'], 1)        
        self.assertTrue(appcomp_returned_dict['app_specific_dict']['field2'])    
        
    def test_application_response_returns_correct_stuff_if_async_json_response_incorrect(self):
        
        """
        Create an incorrect async_json_response file, put it on server, then
        retrieve and verify that what gets returned by application_response()
        is what we expect
        """


        # Create a simple incorrect test async response JSON file and upload it
        # to the server
        # Create JSON request file - it's missing utime_start key
        local_test_async_filepath = '/tmp/asynctest_' + str(uuid.uuid4()) + '.json'
        test_async_response_dict = {
                'status' : 'RUNNING',
                'utime_update' : 1551116960,
                'app_specific_dict' : {'field1' : 1, 'field2' : True}
                }
        
        with open(local_test_async_filepath, 'w') as fh:
            json.dump(test_async_response_dict, fh)

        # Will just put the file in /tmp of the server
        # I can't give it exactly the same name though, because
        # if the client and server are on same machine then I'll have
        # a conflict
        remote_test_async_filepath = local_test_async_filepath + '.remote'
        
        success = self.P._scpput(local_path=local_test_async_filepath, 
                                 remote_path=remote_test_async_filepath)        
        self.assertTrue(success)   # Not really part of this test, but, wth        
        
        # Remove the local copy
        os.remove(local_test_async_filepath)

        appcomp_returned_dict = self.P.application_response(
                async_response_filepath=remote_test_async_filepath)
        
        # appcomp_returned_dict should have 'MESG_ERROR' status, and
        # copy of the dict that got returned (which is the same that we
        # wrote for the test)
        self.assertEqual(appcomp_returned_dict['status'], 'MESG_ERROR')
        received_dict = appcomp_returned_dict['received_dict']
        self.assertEqual(received_dict['status'], 'RUNNING')
        self.assertTrue(received_dict['app_specific_dict']['field2'])   

    def test_scpput_and_scpget_success_barebones(self):

        """
        The next test is nonlinear, in that is uses 
        retrieve_coordinator_dir(), which uses scpget(), so 
        this one will be much like it, but use the raw
        /tmp dir of the server so that it can bypass the
        nonlinear methods.
        
        Create a temporary local file, scp it to the server, then
        scp it back (under a different name) and insure the contents 
        are identical        
        """        
        # Create original file
        orig_filename = str(uuid.uuid4()) + '.teststuff'
        orig_filepath = MSG_SCRATCH_DIR + '/' + orig_filename
        fh = open(orig_filepath, 'w')
        fh.write('This is my silly little ole test')
        fh.close()
        
        # scp it to the server in /tmp dir
        remote_filepath = '/tmp/' + orig_filename
        success = self.P._scpput(local_path=orig_filepath, remote_path=remote_filepath)        
        self.assertTrue(success, msg="scpput success")        
  
        # Get it back, with a different file name
        retrieved_path = orig_filepath + '.retrieved'
        success = self.P._scpget(remote_path=remote_filepath, 
                                 local_path=retrieved_path)
        self.assertTrue(success, msg="scpget success")
                
        self.assertTrue(filecmp.cmp(orig_filepath, retrieved_path), msg="filecmp")        
        
        
    def test_scpput_and_scpget_success(self):
        
        """
        Create a temporary local file, scp it to the server, then
        scp it back (under a different name) and insure the contents 
        are identical
        """

        # Create original file
        orig_filename = str(uuid.uuid4()) + '.teststuff'
        orig_filepath = MSG_SCRATCH_DIR + '/' + orig_filename
        fh = open(orig_filepath, 'w')
        fh.write('This is my silly little ole test')
        fh.close()
        
        # scp it to the server in the coord_request_dir
        self.P.retrieve_coordinator_info()
        coord_request_dir = self.P._coordinator_request_dir()        
        remote_filepath = coord_request_dir + '/' + orig_filename
        success = self.P._scpput(local_path=orig_filepath, remote_path=remote_filepath)        
        self.assertTrue(success)        
  
        # Get it back, with a different file name
        retrieved_path = orig_filepath + '.retrieved'
        success = self.P._scpget(remote_path=remote_filepath, 
                                 local_path=retrieved_path)
        self.assertTrue(success)
                
        self.assertTrue(filecmp.cmp(orig_filepath, retrieved_path))        


    def test_scpput_returns_false_on_bad_remote(self):
        
        # Create original file
        orig_filename = str(uuid.uuid4()) + '.teststuff'
        orig_filepath = MSG_SCRATCH_DIR + '/' + orig_filename
        fh = open(orig_filepath, 'w')
        fh.write('This is my silly little ole test')
        fh.close()        

        # scp to a bad server dir
        remote_filepath = '/stupiddirthatwillneverexist/' + orig_filename
        success = self.P._scpput(local_path=orig_filepath, remote_path=remote_filepath)        
        self.assertFalse(success)     


    def test_scpput_returns_false_on_bad_local(self):
        
        orig_filepath = '/afilename/thatwill/neverexist.txt'

        # scp it to the server in the coord_request_dir
        self.P.retrieve_coordinator_info()
        coord_request_dir = self.P._coordinator_request_dir()     
        remote_filepath = coord_request_dir + '/test.txt'      
        success = self.P._scpput(local_path=orig_filepath, remote_path=remote_filepath)        
        self.assertFalse(success)     


    def test_scpget_returns_false_on_bad_remote(self):
        
        remote_filepath = '/afilename/thatwill/neverexist.txt'
        local_filepath = MSG_SCRATCH_DIR + '/temp.txt'
        success = self.P._scpget(remote_path=remote_filepath, local_path=local_filepath)        
        self.assertFalse(success)             

    def test_scpget_returns_false_on_bad_local(self):
        
        
        # First, we'll need to put a known good file on the remote
        orig_filename = str(uuid.uuid4()) + '.teststuff'
        orig_filepath = MSG_SCRATCH_DIR + '/' + orig_filename
        fh = open(orig_filepath, 'w')
        fh.write('This is my silly little ole test')
        fh.close()
        
        # scp it to the server in the coord_request_dir
        self.P.retrieve_coordinator_info()
        coord_request_dir = self.P._coordinator_request_dir()        
        remote_filepath = coord_request_dir + '/' + orig_filename
        success = self.P._scpput(local_path=orig_filepath, remote_path=remote_filepath)        
        self.assertTrue(success)       # Might as well make sure this worked 
        
        # Try to retrieve with a bad local dir
        retrieved_path = '/this/is/another/path/that/will/never/exist/i/hope.txt'
        success = self.P._scpget(remote_path=remote_filepath, 
                                 local_path=retrieved_path)
        self.assertFalse(success)


    def test_async_response_success(self):
        
        # Create a simple test async response JSON file and upload it
        # to the server
        # Create JSON request file
        local_test_async_filepath = '/tmp/asynctest_' + str(uuid.uuid4()) + '.json'
        test_async_response_dict = {
                'status' : 'RUNNING',
                'message' : 'This is the test message'
                }
        
        with open(local_test_async_filepath, 'w') as fh:
            json.dump(test_async_response_dict, fh)

        # Will just put the file in /tmp of the server
        # I can't give it exactly the same name though, because
        # if the client and server are on same machine then I'll have
        # a conflict
        remote_test_async_filepath = local_test_async_filepath + '.remote'
        
        success = self.P._scpput(local_path=local_test_async_filepath, 
                                 remote_path=remote_test_async_filepath)        
        self.assertTrue(success)   # Not really part of this test, but, wth        
        
        # Remove the local copy
        os.remove(local_test_async_filepath)
        
        # Call the method, with the full server path as arg
        return_dict = self.P.async_response(
                async_response_filepath=remote_test_async_filepath) 
        async_response_dict = return_dict['async_response_dict']
        
        # Test returned fields
        self.assertTrue(return_dict['retrieve_success'])
        self.assertEqual(async_response_dict['status'], 'RUNNING')
        self.assertEqual(async_response_dict['message'],
                         'This is the test message')        


    def test_async_response_returns_correct_mesg_on_failed_scpget(self):
        
        """
        Call async_response() with a bad remote path which scp will
        fail on, and verify correct responses
        """
        
        bad_path = '/You/gotta/be/kidding!'
        
        return_dict = self.P.async_response(
                async_response_filepath=bad_path) 
        async_response_dict = return_dict['async_response_dict']
        
        # Test returned fields
        self.assertFalse(return_dict['retrieve_success'])
        self.assertIsNone(async_response_dict)


    def test_async_response_returns_correct_mesg_on_bad_json(self):
        
        """
        Create a simple text file - non JSON - put it on server, then 
        call async_response() and insure that the file is retrieved,
        but JSON extraction fails
        """

        # Create the bogus JSON file
        local_test_async_filepath = '/tmp/asynctest_' + str(uuid.uuid4()) + '.json'
        fh = open(local_test_async_filepath, 'w')
        fh.write("This is really, really bogus, man")
        fh.close()

        # Will just put the file in /tmp of the server
        # I can't give it exactly the same name though, because
        # if the client and server are on same machine then I'll have
        # a conflict
        remote_test_async_filepath = local_test_async_filepath + '.remote'
        
        success = self.P._scpput(local_path=local_test_async_filepath, 
                                 remote_path=remote_test_async_filepath)        
        self.assertTrue(success)   # Not really part of this test, but, wth        
        
        # Remove the local copy
        os.remove(local_test_async_filepath)
        
        # Call the method, with the full server path as arg
        return_dict = self.P.async_response(
                async_response_filepath=remote_test_async_filepath) 
        async_response_dict = return_dict['async_response_dict']
        
        # Test returned fields
        self.assertTrue(return_dict['retrieve_success'])
        self.assertIsNotNone(return_dict['retrieved_localpath'])
        self.assertIsNone(async_response_dict)        
