import unittest.mock
import unittest

import json
import os
import shutil
import subprocess
import time
import uuid


import Coordinator

coordinator_exec_path_env = os.environ.get('COORDINATOR_EXEC_PATH')
if coordinator_exec_path_env is None:
    COORDINATOR_EXEC_PATH = './coordinator'
else:
    COORDINATOR_EXEC_PATH = coordinator_exec_path_env    
    
    
class coordinator_Functional_Test(unittest.TestCase):

    
    """
    This is a driver for the "coordinator" executable testing
    
    The Coordinator in this test is used to obtain independent
    information about the class so that it can be compared with
    what coordinator gets through its commandline
    """
          
        
    def test_coordinator_exec_path_good(self):
        
        is_good = os.path.isfile(COORDINATOR_EXEC_PATH) and \
                    os.access(COORDINATOR_EXEC_PATH, os.X_OK)        
        self.assertTrue(is_good)
        


    def test_coordinator_sysinfo(self):

        """        
        Get sysinfo using the coordinator CLI first, which will get default
        values from the Coordinator class
        """

        the_command = [COORDINATOR_EXEC_PATH, 'sysinfo']
        proc = subprocess.Popen(the_command, stdout=subprocess.PIPE)
        output = proc.stdout.read().strip()   # Should be a path to json file
        with open(output, 'r') as fh:
            response_dict = json.load(fh)
            
        direct_request_dir = response_dict['request_dir']
        direct_stdout_delimiter = response_dict['stdout_delimiter']
        
        # Now, also get the same stuff directly from the class, for comparing
        C = Coordinator.Coordinator()
        class_request_dir = C.incoming_requests_dir()
        class_stdout_delimiter = C.stdout_delimiter()

        self.assertEqual(direct_request_dir, class_request_dir)
        self.assertEqual(direct_stdout_delimiter, class_stdout_delimiter)
        
        
    def test_coordinator_jsonrequest(self):
        
        """
        Make a typical jsonrequest and insure that I can retrieve the
        sync response and extract the status of OK
        """
        
        # First, get the delimiter that will be used
        the_command = [COORDINATOR_EXEC_PATH, 'delimiter']
        proc = subprocess.Popen(the_command, stdout=subprocess.PIPE)
        delimiter = proc.stdout.read().strip()   # Should be a path to json file


        # Make the test request
        the_command = [COORDINATOR_EXEC_PATH, 'jsonrequest',
                       'test/functional/testsleep_request_1.json']
        proc = subprocess.Popen(the_command, stdout=subprocess.PIPE)
        response_str = proc.stdout.read().strip()

        # Extract the sync response filepath        
        delim_length = len(delimiter)
        idx_first = delim_length
        idx_last = -(delim_length+1)
        sync_responsepath = response_str[idx_first:idx_last+1]    
        
        # Extract the JSON
        with open(sync_responsepath, 'r') as fh:
            sync_response_dict = json.load(fh)
            
        # Get the status out and verify OK
        status = sync_response_dict['status']
        self.assertEqual(status, 'OK')

        