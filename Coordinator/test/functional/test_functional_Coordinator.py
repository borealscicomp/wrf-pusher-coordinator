import unittest.mock
import unittest

import json
import os
import shutil
import time
import uuid

import Coordinator


REQUESTS_DIR = '/tmp/' + str(uuid.uuid4())
RESPONSES_DIR = '/tmp/' + str(uuid.uuid4())
LOGS_DIR = '/tmp/' + str(uuid.uuid4())
DELIM = '!!!***!!!'

class Coordinator_Functional_Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):

        cls.C = Coordinator.Coordinator(
                incoming_requests_dir=REQUESTS_DIR,
                outgoing_responses_dir=RESPONSES_DIR,
                logs_dir=LOGS_DIR,
                stdout_delimiter=DELIM)        

    @classmethod
    def tearDownClass(cls):
        
        # Remove the directories.  Give the async part of the tests time to 
        # finish before deleting the directories
        #time.sleep(10)
        pass

        shutil.rmtree(REQUESTS_DIR)
        shutil.rmtree(RESPONSES_DIR)
        shutil.rmtree(LOGS_DIR)


        
        
    def test_dummy(self):
        self.assertTrue(True)
        

    def test_successful_async_process_sync_response(self):


        """
        The idea of this functional test is to invoke the pre-canned
        asynchronous process, TestSleep.py::testsleep(), and to verify
        that the sync response gives us a status of 'OK'
        """
        
        # Create request JSON
        request_json = {"task": 
            {"command_str": 
                "test/functional/TestSleep.py --method testsleep \
                --iterations 3 --sleepsecs 2"}}
        request_filepath = self.C.incoming_requests_dir() + '/' + str(uuid.uuid4())
        with open(request_filepath, 'w') as fh:
            json.dump(request_json, fh)
        fh.close()
        
        # Submit request
        stdout_response = self.C.async_process(request_path=request_filepath)
        print('stdout_response: %s' % stdout_response)
        
        
        # Somehow capture the stdout that it issues??  In the above, I just
        # used the async_process() "test" return of stdout, so I don't have
        # to try to actually capture it, though I should try at some point.


   
        # Extract the file from the delimiters.  This is a bit of a hack, but
        # I know exactly that I should expect delimiter + path + delimiter
        delim_length = len(self.C.stdout_delimiter())
        idx_first = delim_length
        idx_last = -(delim_length+1)
        sync_responsepath = stdout_response[idx_first:idx_last+1]
        self.assertTrue(os.path.isfile(sync_responsepath))
        
        # Then, open this file, extract the JSON, and check for status of OK
        with open(sync_responsepath, 'r') as fh:
            response_dict = json.load(fh)
        fh.close()
        self.assertEqual(response_dict['status'], 'OK')


    def test_successful_async_process_async_responses(self):


        """
        The idea of this functional test is to invoke the pre-canned
        asynchronous process, TestSleep.py::testsleep(), and to verify
        that the async_response JSON file gives us correct responses
        over time.
        
        This has to be coordinated with the actual timings invoked in
        the request_json, below.
        
        I should expect that, initally, after I have the async_response_filepath,
        I should be able to see a status of RUNNING.  However, because the process
        
        
        After five seconds I should see a status of COMPLETE, and I should
        see 
        """
        
        # Create request JSON
        request_json = {"task": 
            {"command_str": 
                "test/functional/TestSleep.py --method testsleep \
                --iterations 3 --sleepsecs 3"}}
        request_filepath = self.C.incoming_requests_dir() + '/' + str(uuid.uuid4())
        with open(request_filepath, 'w') as fh:
            json.dump(request_json, fh)
        fh.close()
        
        # Submit request
        stdout_response = self.C.async_process(request_path=request_filepath)
        print('stdout_response: %s' % stdout_response)
        
        
        # Somehow capture the stdout that it issues??  In the above, I just
        # used the async_process() "test" return of stdout, so I don't have
        # to try to actually capture it, though I should try at some point.
        
        # Extract the file from the delimiters.  This is a bit of a hack, but
        # I know exactly that I should expect delimiter + path + delimiter
        delim_length = len(self.C.stdout_delimiter())
        idx_first = delim_length
        idx_last = -(delim_length+1)
        sync_responsepath = stdout_response[idx_first:idx_last+1]
        #self.assertTrue(os.path.isfile(sync_responsepath))
        
        # Then, open this file, extract the JSON and get the async response path
        with open(sync_responsepath, 'r') as fh:
            response_dict = json.load(fh)
        fh.close()
        #self.assertEqual(response_dict['status'], 'OK')
        async_response_filepath = response_dict['async_response_filepath']
        
        # Open it 5 seconds in and look for status of RUNNING
        time.sleep(5)
        with open(async_response_filepath, 'r') as fh:
            response_dict = json.load(fh)
        fh.close()        
        self.assertEqual(response_dict['status'], 'RUNNING')

        # Open it another 6 seconds in and look for status of COMPLETE
        time.sleep(5)
        with open(async_response_filepath, 'r') as fh:
            response_dict = json.load(fh)
        fh.close()        
        self.assertEqual(response_dict['status'], 'COMPLETE')


  
    def test_successful_app_compliant_async_process_async_responses(self):


        """
        The idea of this functional test is to invoke the pre-canned
        asynchronous process, TestSleep.py::appcomp_testsleep(), and to verify
        that the async_response JSON file gives us correct responses
        over time.
        
        This has to be coordinated with the actual timings invoked in
        the request_json, below.
        
        I should expect that, initally, after I have the async_response_filepath,
        I should be able to see a status of RUNNING.  However, because the process
        
        
        After five seconds I should see a status of COMPLETE, and I should
        see 
        """
        
        # Create request JSON
        request_json = {"task": 
            {"command_str": 
                "test/functional/TestSleep.py --method appcomp_testsleep \
                --iterations 3 --sleepsecs 3"}}
        request_filepath = self.C.incoming_requests_dir() + '/' + str(uuid.uuid4())
        with open(request_filepath, 'w') as fh:
            json.dump(request_json, fh)
        fh.close()
        
        # Submit request
        stdout_response = self.C.async_process(request_path=request_filepath)
        print('stdout_response: %s' % stdout_response)
        
        
        # Somehow capture the stdout that it issues??  In the above, I just
        # used the async_process() "test" return of stdout, so I don't have
        # to try to actually capture it, though I should try at some point.
        
        # Extract the file from the delimiters.  This is a bit of a hack, but
        # I know exactly that I should expect delimiter + path + delimiter
        delim_length = len(self.C.stdout_delimiter())
        idx_first = delim_length
        idx_last = -(delim_length+1)
        sync_responsepath = stdout_response[idx_first:idx_last+1]
        #self.assertTrue(os.path.isfile(sync_responsepath))
        
        # Then, open this file, extract the JSON and get the async response path
        with open(sync_responsepath, 'r') as fh:
            response_dict = json.load(fh)
        fh.close()
        #self.assertEqual(response_dict['status'], 'OK')
        async_response_filepath = response_dict['async_response_filepath']
        
        # Open it 5 seconds in and look for status of RUNNING
        time.sleep(5)
        with open(async_response_filepath, 'r') as fh:
            response_dict = json.load(fh)
        fh.close()        
        self.assertEqual(response_dict['status'], 'RUNNING')

        # Open it another 6 seconds in and look for status of COMPLETE
        time.sleep(5)
        with open(async_response_filepath, 'r') as fh:
            response_dict = json.load(fh)
        fh.close()        
        self.assertEqual(response_dict['status'], 'COMPLETE')
        
        # Test that it also has the other fields expected from an application-
        # compliant exchange
        
        # In this time fields, I'll simply make sure that the unix times are
        # greater than a "somewhat" current time.  The time 1551116954 is
        # approximately 2019-02-25_1800 UTC
        self.assertGreater(response_dict['utime_start'], 1551116954)
        self.assertGreater(response_dict['utime_update'], 
                           response_dict['utime_start'])
        
        self.assertEqual("All is groovy!", 
                         response_dict['app_specific_dict']['message'])
        

        