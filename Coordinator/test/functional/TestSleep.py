#!/usr/bin/env python

import argparse
import datetime
import json
import logging
import sys
import time


class TestSleep(object):


    """
    This is a test class with simple test methods.  It's meant to be 
    something that might be run asynchronously by running

    ./TestSleep.py --method testsleep 
                   --jsonout /tmp/async_response_testsleep.json
                   --iterations 3
                   --sleepsecs 2   

    The testsleep() method will iterate as instructed by args (or defaults),
    writing a new async jsonout file at each step with a status of RUNNING
    or when done, COMPLETED.  Additionally, the method will maintain a list
    of unix times at each iteration, and write the new list in to the async
    jsonout file.

    appcomp_testsleep() does the same as testsleep(), but is "application
    compliant " which means it will create a "standard" JSON response.

    This gives a driving test program something predictable to work with
    """
    def __init__(self):
        pass

    def testsleep(self, sleep_secs=2, iterations=3,
                  output_json_file=None):

        output_dict = {}
        output_dict['status'] = 'RUNNING'
        output_dict['modified_times'] = None
        output_dict['message'] = None
        

        # Make a list of modification times
        modified_times = []

        for i in range(iterations):
            logging.info('Sleeping for %d secs' % sleep_secs)
            time.sleep(sleep_secs)

            new_time = int( datetime.datetime.utcnow().strftime('%s') )
            modified_times.append(new_time)
            output_dict['modified_times'] = modified_times
        
            fh = open(output_json_file, 'w')
            json.dump(output_dict, fh)
            fh.close()

        output_dict['status'] = 'COMPLETE'
        output_dict['message'] = 'All is groovy!'
        
        fh = open(output_json_file, 'w')
        json.dump(output_dict, fh)
        fh.close()

        logging.info('Done.  Wrote to: %s' % output_json_file)

        return True

    def appcomp_testsleep(self, sleep_secs=2, iterations=3,
                  output_json_file=None):

        # List of modified times
        modified_times = []
        
        output_dict = {}
        output_dict['status'] = 'RUNNING'
        output_dict['utime_start'] = self._unixtime()
        output_dict['utime_update'] = self._unixtime()
        output_dict['app_specific_dict'] = {'modified_times' : modified_times}
        
        for i in range(iterations):
            logging.info('Sleeping for %d secs' % sleep_secs)
            time.sleep(sleep_secs)

            new_time = int( datetime.datetime.utcnow().strftime('%s') )
            modified_times.append(new_time)
            output_dict['app_specific_dict']['modified_times'] = modified_times
            output_dict['utime_update'] = self._unixtime()
        
            fh = open(output_json_file, 'w')
            json.dump(output_dict, fh)
            fh.close()

        output_dict['status'] = 'COMPLETE'
        output_dict['app_specific_dict']['message'] = 'All is groovy!'
        
        fh = open(output_json_file, 'w')
        json.dump(output_dict, fh)
        fh.close()

        logging.info('Done.  Wrote to: %s' % output_json_file)

        return True

    def _unixtime(self):
        import calendar
        import datetime
        return calendar.timegm(datetime.datetime.utcnow().utctimetuple())


def main():
    
    args = getargs()

    TS = TestSleep()

    if args.method == 'testsleep':

        args_dict = {}
        args_dict['output_json_file'] = args.jsonout
        if args.iterations: 
            args_dict['iterations'] = args.iterations 
        if args.sleepsecs: 
            args_dict['sleep_secs'] = args.sleepsecs 
        success = TS.testsleep(**args_dict)
    elif args.method == 'appcomp_testsleep':
        args_dict = {}
        args_dict['output_json_file'] = args.jsonout
        if args.iterations: 
            args_dict['iterations'] = args.iterations 
        if args.sleepsecs: 
            args_dict['sleep_secs'] = args.sleepsecs 
        success = TS.appcomp_testsleep(**args_dict)        
    else:
        logging.error('No valid method specified')
        return

def getargs():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--iterations", type=int)
    parser.add_argument("-j", "--jsonout", required=True)
    parser.add_argument("-m", "--method", required=True)
    parser.add_argument("-s", "--sleepsecs", type=int)
    args = parser.parse_args()

    return args

if __name__=="__main__":

    main()

