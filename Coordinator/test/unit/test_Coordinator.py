import unittest.mock
import unittest

import json
import shutil
import uuid

import Coordinator


REQUESTS_DIR = '/tmp/' + str(uuid.uuid4())
RESPONSES_DIR = '/tmp/' + str(uuid.uuid4())
LOGS_DIR = '/tmp/' + str(uuid.uuid4())
DELIM = '!!!***!!!'

class Coordinator_Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):

        cls.C = Coordinator.Coordinator(
                incoming_requests_dir=REQUESTS_DIR,
                outgoing_responses_dir=RESPONSES_DIR,
                logs_dir=LOGS_DIR,
                stdout_delimiter=DELIM)        

    @classmethod
    def tearDownClass(cls):
        
        # Remove the directories
        shutil.rmtree(REQUESTS_DIR)
        shutil.rmtree(RESPONSES_DIR)
        shutil.rmtree(LOGS_DIR)


        
        
    def test_dummy(self):
        self.assertTrue(True)
        
        
    def test_stdout_delimiter(self):        
        self.assertEqual(DELIM, self.C.stdout_delimiter())
        
    def test_incoming_requests_dir(self):        
        self.assertEqual(REQUESTS_DIR, self.C.incoming_requests_dir())        

    def test_outgoing_responses_dir(self):        
        self.assertEqual(RESPONSES_DIR, self.C.outgoing_responses_dir()) 
        
        
    def test_post_json_response(self):
        """Create simple dict, post to JSON file, then retrieve/compare"""
        
        test_dict = {'test_name':'Don', 
                     'subdict':{'testsubdictname':'Morton'}}
        
        response_json_path = self.C.post_json_response(response_dict=test_dict)
        
        fh = open(response_json_path, 'r')
        retrieved_dict = json.load(fh)
        fh.close()
        
        self.assertDictEqual(test_dict, retrieved_dict)

    def test_post_json_response_returns_none_for_bad_dict(self):
        """Create simple bad dict, and insure that failure to create
        JSON results in return of None"""
        
        test_dict = 3.14159
        
        response_json_path = self.C.post_json_response(response_dict=test_dict)
        
        self.assertIsNone(response_json_path)        
        
        
    def test_async_process_returns_fail_for_bad_request_path(self):

        """Insure that bad request path results in access to JSON
        file with status==FAIL"""

        bad_path = '/tmp/pmt/' + str(uuid.uuid4())        
        
        sync_response_dict = self.C.async_process(request_path=bad_path)
        
        self.assertEqual(sync_response_dict['status'], 'FAIL')
        
        
    def test_async_process_returns_fail_for_empty_request_path(self):

        """Insure that empty request path results in access to JSON
        file with status==FAIL"""

        sync_response_dict = self.C.async_process()
        
        self.assertEqual(sync_response_dict['status'], 'FAIL')        
        
        
    def test_async_process_returns_fail_for_nondict_request(self):

        """Insure that request path containing JSON for invalid dict  
        results in access to JSON file with status==FAIL"""
        
        
        # Create request file with bad dict
        bad_dict_filepath = REQUESTS_DIR + '/' + str(uuid.uuid4()) + '.json'
        bad_dict = 'Hey man, this is really, really, really crappy dict'
        fh = open(bad_dict_filepath, 'w')
        json.dump(bad_dict, fh)
        fh.close()

        
    def test_async_process_returns_fail_for_invalid_dict_keys(self):

        """Insure that if the request_dict does not have the expected
        keys ['task']['command_str'], than status in JSON response will
        be FAIL"""
        
        
        # Create request file with bad JSON
        bad_dict_filepath = REQUESTS_DIR + '/' + str(uuid.uuid4()) + '.json'
        bad_dict = {'task':{'command_str_with_bad_key':'arbitrary string'}}
        fh = open(bad_dict_filepath, 'w')
        json.dump(bad_dict, fh)
        fh.close()      


        sync_response_dict = self.C.async_process(request_path=bad_dict_filepath)
        
        self.assertEqual(sync_response_dict['status'], 'FAIL')        
                










        
        