
import datetime
import json
import logging
import os
import subprocess
import sys
import uuid


coordinator_msgs_root_env = os.environ.get('COORDINATOR_MSGS_ROOT')
if coordinator_msgs_root_env is None:
    COORDINATOR_MSGS_ROOT = '/tmp/CoordinatorMessages'
else:
    COORDINATOR_MSGS_ROOT = coordinator_msgs_root_env

INCOMING_REQUESTS_DIR = COORDINATOR_MSGS_ROOT + '/coordinator_incoming_requests'
OUTGOING_RESPONSES_DIR = COORDINATOR_MSGS_ROOT + '/coordinator_outgoing_responses'
LOGS_DIR = COORDINATOR_MSGS_ROOT + '/coordinator_logs'

LOGGING_LEVEL = logging.DEBUG



STDOUT_DELIMITER= '$|$|$'

# Response codes to be sent back to client in stdout strings
RESPONSE_GOOD = 0
RESPONSE_FAILED = 2

"""
LOGGER = logging.getLogger('Coordinator')
handler = logging.FileHandler(MY_LOGGER_FILE)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler.setFormatter(formatter)
LOGGER.addHandler(handler)
LOGGER.setLevel(logging.INFO)
"""


class Coordinator(object):

    def __init__(self, incoming_requests_dir=INCOMING_REQUESTS_DIR,  
                 outgoing_responses_dir=OUTGOING_RESPONSES_DIR,
                 logs_dir=LOGS_DIR,
                 stdout_delimiter=STDOUT_DELIMITER):

        # Make sure the log directory exists
        if not os.path.isdir(logs_dir):
            try:
                os.makedirs(logs_dir, 0o755)

                #LOGGER.info('Created dir: %s' % incoming_requests_dir)
            except:  
                raise Exception('Failed to create: %s' % 
                                 logs_dir)
        self._logs_dir = logs_dir

        # Set up logging
        logger_filename = self._logs_dir + '/Coordinator.log'
        self._LOGGER = logging.getLogger('Coordinator')
        handler = logging.FileHandler(logger_filename)
        formatter = logging.Formatter(
                '%(levelname)-8s %(asctime)s COORD : %(funcName)s:%(lineno)d--%(message)s')
        handler.setFormatter(formatter)
        self._LOGGER.addHandler(handler)
        self._LOGGER.setLevel(LOGGING_LEVEL)




        # Make sure the message directories exist
        if not os.path.isdir(incoming_requests_dir):
            try:
                os.makedirs(incoming_requests_dir, 0o755)

                self._LOGGER.info('Created dir: %s' % incoming_requests_dir)
            except:  
                raise Exception('Failed to create: %s' % 
                                 incoming_requests_dir)
        self._incoming_requests_dir = incoming_requests_dir

        if not os.path.isdir(outgoing_responses_dir):
            try:
                os.makedirs(outgoing_responses_dir, 0o755)

                self._LOGGER.info('Created dir: %s' % outgoing_responses_dir)
            except:  
                raise Exception('Failed to create: %s' % 
                                 outgoing_responses_dir)
        self._outgoing_responses_dir = outgoing_responses_dir
        
        
        self._stdout_delimiter=stdout_delimiter




    def async_process(self, request_path=None):
        
        """
        Intended for handling an asynchronous task
        
        * Read request from request_path and decode JSON (which should contain
          task to run as well as arguments, and anything else needed)
        * Set up a quick synchronous response dict and JSON filename
        * Set up an async_task_response JSON filepath that task will write
          later, and put this name in the sync_response dict
        * Write the sync_response JSON file
        * Write the sync_response path to stdout with delimiters, for client
        * Spawn the task, no blocking, no parent, then return (maybe I
          want to return an informative dict so that whoever calls this
          method could print to log or something)
        
        
        If anything goes wrong, the returned sync_response_dict should include
        key status of 'FAIL' with a relevant status_message.
        """
        
        self._LOGGER.debug('request_path: %s' % request_path)


        sync_response_dict = {'status':None, 'status_msg':None}
        # extract the json
        try:
            fh = open(request_path, 'r')
            request_dict = json.load(fh)
            fh.close
        except:
            sync_response_dict['status'] = 'FAIL'
            sync_response_dict['status_msg'] = 'Failed to extract JSON from: ' + \
                                            str(request_path)
            return sync_response_dict
    
        self._LOGGER.debug('request_dict: %s' % request_dict)
    

        # Next, insure we have retrieved a valid dict, and that it has
        # keys ['task']['command_str']
        if type(request_dict) != dict:
            sync_response_dict['status'] = 'FAIL'
            sync_response_dict['status_msg'] = 'request_dict not a dict: ' + \
                                            str(request_dict)
            return sync_response_dict            
        try:
            key_test = request_dict['task']['command_str']
        except:
            sync_response_dict['status'] = 'FAIL'
            sync_response_dict['status_msg'] = \
                  'request_dict missing task and/or command_str keys: ' + \
                                            str(request_dict)
            return sync_response_dict                        



        # Create async_task_response JSON filepath
        timestr = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S.%f')
        filename = 'async_response_' + timestr + '_' + \
                   str(uuid.uuid4()) + '.json'
        async_response_filepath = self._outgoing_responses_dir +\
                                  '/' + filename

        sync_response_dict['async_response_filepath'] = async_response_filepath

        # Extract the command from the request json and add the response
        # filepath
        command_str = request_dict['task']['command_str']
        # For now just append it on, but in the future maybe see if it already
        # exists
        command_str += ' --jsonout ' + str(async_response_filepath)
        sync_response_dict['command_str'] = command_str


        # Create sync_response_filepath, write the dict to it in JSON,
        # then print this path as stdout between delimiters
        # THIS IS IMPORTANT - if client doesn't get this exactly, it
        # becomes lost
        timestr = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S.%f')
        filename = 'sync_response_' + timestr + '_' + \
                   str(uuid.uuid4()) + '.json'
        sync_response_filepath = self._outgoing_responses_dir +\
                                  '/' + filename

        sync_response_dict['status'] = 'OK'
        
        fh = open(sync_response_filepath, 'w')
        json.dump(sync_response_dict, fh)
        fh.close()
        self._LOGGER.info('Wrote sync JSON to file: %s' % sync_response_filepath)

        # And here's the stdout response for the client
        stdout_response = self.stdout_delimiter() + \
                            sync_response_filepath + \
                            self.stdout_delimiter()
        self._LOGGER.debug('stdout_response: %s' % stdout_response)
        print(stdout_response)



        # Finally, let's launch it nonblocking and exit
        command_list = command_str.split()
        p = subprocess.Popen(command_list,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)





        
        
        
        
        # This is only put in here for local testing where it may be difficult
        # to retrieve the stdout response from a command line invocation.
        # This method should not return anything of value in operational mode
        return stdout_response

    def sync_respond(self, request_path=None):

        """
        
        THIS IS CURRENTLY NOT IMPLEMENTED.  I'M LEAVING IT HERE IN CASE
        I DECIDE IT'S WORTH IMPLEMENTING SOME DAY
        
        Intended for quick response - 

            * Read request from request_path and decode JSON
            * Assume there is a "command" field in it
            * Create a unique response filepath
            * Execute the command and direct output to response filepath
            * Respond to client via stdout with success code and response path
        """
        sys.exit('sync_respond() is not implemented!!!')



        response_code = RESPONSE_GOOD

        # Create response filename
        timestr = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S.%f')
        filename = timestr + '_' + str(uuid.uuid4()) + '.json'
        response_filepath = self._outgoing_responses_dir + '/' + filename 

        the_stdout_reply = STDOUT_DELIMITER + str(response_code) + STDOUT_DELIMITER
        the_stdout_reply += response_filepath + STDOUT_DELIMITER

        print(the_stdout_reply)
        
    def stdout_delimiter(self):
        
        return self._stdout_delimiter
        

    def incoming_requests_dir(self):
        
        return self._incoming_requests_dir
    
    
    def outgoing_responses_dir(self):
        
        return self._outgoing_responses_dir


    def post_json_response(self, response_dict=None):
        
        """
        Creates a unique JSON filename, then writes response_dict into
        it as JSON.
        
        Returns full path to the JSON filename
        
        If response_dict is not a dict, then None is returned
        """

        if type(response_dict) != dict:
            return None
        
        # Create response filename
        timestr = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S.%f')
        filename = timestr + '_' + str(uuid.uuid4()) + '.json'
        response_filepath = self.outgoing_responses_dir() + '/' + filename 
        self._LOGGER.info('response_filepath: %s' % response_filepath)

        # Make sure we have "something" to write, even if it's empty
        if response_dict is None:
            response_dict = {}

        # Write the dict to the JSON file
        fh = open(response_filepath, 'w')
        json.dump(response_dict, fh)
        fh.close()
        self._LOGGER.info('Wrote JSON to file: %s' % response_filepath)

        return response_filepath

if __name__=="__main__":

    C = Coordinator()
    #C.sync_respond()
    
    the_dict = {'field':'hello!!!'}
    #response_json = C.post_outgoing_response(response_dict=the_dict)
    response_json = C.post_json_response()
    print('response_json: %s' % response_json)


