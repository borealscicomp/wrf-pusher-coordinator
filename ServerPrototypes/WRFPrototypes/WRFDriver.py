# -*- coding: utf-8 -*-


DISTRO_ROOT = '/home/ubuntu/'

import sys
import time

sys.path.insert(0, DISTRO_ROOT)


import Pusher.Pusher as Pusher


"""
# These should be defined in a module eventually




#COORDINATOR_PATH = '/home/morton/git/ssh-push-coord/'
COORDINATOR_PATH = DISTRO_ROOT + 'Coordinator/coordinator'

#METGRIDRUN_PATH = '/home/morton/git/ssh-push-coord/'
METGRIDRUN_PATH = DISTRO_ROOT + 'ServerPrototypes/WRFPrototypes/MetGridRun.py'

# alaskawx
SSH_COMMAND = '/usr/bin/ssh -p 51459'
SCP_COMMAND = '/usr/bin/scp -P 51459'

# polaris
#SSH_COMMAND = '/usr/bin/ssh'
#SCP_COMMAND = '/usr/bin/scp'

COORDINATOR_USER_AND_HOST = "localhost"  # often user@hostname.fqdn 
LOCAL_MSG_SCRATCH_DIR = '/tmp'
"""

class WRFDriver(object):
    
    def __init__(self, coordinator_path=None,
                 ssh_command=None, scp_command=None,
                 userhost=None,
                 local_msg_scratch_dir=None
                 ):
        
        self._P = Pusher.Pusher(coordinator_path=coordinator_path,
                          ssh_command=ssh_command,
                          scp_command=scp_command,
                          userhost=userhost,
                          local_msg_scratch_dir=local_msg_scratch_dir)

        self._metgrid_async_response_filepath = None
    
    
    
    def start_metgrid(self, metgridrun_path=None,
                      starttime=None, endtime=None,
                    interval=None):
        
        # These are used to simulate work, etc.
        SLEEP_INTVL = 1
        FAKE_ERROR = True
        
        # Form the command
        the_command = metgridrun_path
        the_command += ' --starttime ' + str(starttime)
        the_command += ' --endtime ' + str(endtime)
        the_command += ' --interval ' + str(interval)
        the_command += ' --sleepintvl ' + str(SLEEP_INTVL)
        #if FAKE_ERROR:
        #    the_command += ' --fakeerror '
        print('the_command: ' + str(the_command))
        
        response_dict = self._P.json_request(command_str=the_command)
        
        self._metgrid_async_response_filepath = response_dict['async_response_filepath']
            
        return response_dict

    def status_metgrid(self):
        
        async_response_dict = self._P.application_response(
                async_response_filepath=self._metgrid_async_response_filepath)
        
        return async_response_dict
    
if __name__ == "__main__":
    
    STARTTIME = 20190214180000
    ENDTIME = 20190214210000
    INTERVAL = 3600
    
    
    
    #COORDINATOR_PATH = '/home/morton/git/ssh-push-coord/'
    COORDINATOR_PATH = DISTRO_ROOT + 'Coordinator/coordinator'
    
    #METGRIDRUN_PATH = '/home/morton/git/ssh-push-coord/'
    METGRIDRUN_PATH = DISTRO_ROOT + 'ServerPrototypes/WRFPrototypes/MetGridRun.py'
    
    # alaskawx
    SSH_COMMAND = '/usr/bin/ssh -i /home/morton/.AWSKeys/GenericTesting.pem'
    SCP_COMMAND = '/usr/bin/scp -i /home/morton/.AWSKeys/GenericTesting.pem'
    
    # polaris
    #SSH_COMMAND = '/usr/bin/ssh'
    #SCP_COMMAND = '/usr/bin/scp'
    
    COORDINATOR_USER_AND_HOST = "ubuntu@52.40.170.110"  # often user@hostname.fqdn 
    LOCAL_MSG_SCRATCH_DIR = '/tmp'    
    
    
    
    W = WRFDriver(coordinator_path=COORDINATOR_PATH,
                 ssh_command=SSH_COMMAND, scp_command=SCP_COMMAND,
                 userhost=COORDINATOR_USER_AND_HOST,
                 local_msg_scratch_dir=LOCAL_MSG_SCRATCH_DIR)
    
    sync_response_dict = W.start_metgrid(metgridrun_path=METGRIDRUN_PATH,
                                        starttime=STARTTIME, 
                                         endtime=ENDTIME,
                                         interval=INTERVAL)
        
        
    print('sync_response_dict: ' + str(sync_response_dict))        
    
    
    # Get the status and the async json response path, if available
    sync_status = sync_response_dict['status']
    async_response_filepath = sync_response_dict['async_response_filepath'] 
    
    if sync_status == 'OK':
        # Probe around for a while
        TIMEOUT_SECS = 60
        seconds = 0
        while seconds < TIMEOUT_SECS:
            async_response_dict = W.status_metgrid()
            print('async_response_dict: ' + str(async_response_dict))
            if async_response_dict['status'] != 'RUNNING':
                break
            time.sleep(10)
            seconds += 10
    
    
    
    
    
    
    
    
    
    
        
        
