#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 02:17:58 2019

@author: morton
"""


import argparse
import datetime
import json
import logging
import time


logging.basicConfig(level=logging.DEBUG, filename='/tmp/MetGridRun.log')




def main():
    args = getargs()
    
    logging.debug("args: " + str(args))

    stime = str2datetime( args.starttime )
    logging.debug('stime: ' + str(stime))

    etime = str2datetime( args.endtime )
    logging.debug('etime: ' + str(etime))
    
    intvl_seconds = int(args.interval)
    logging.debug('intvl_seconds: %d' % intvl_seconds)
    
    sleep_seconds = int(args.sleepintvl)
    logging.debug('sleep_seconds: %d' % sleep_seconds)    
    
    async_jsonout_filepath = args.jsonout    
    logging.debug('async_jsonout_filepath: %s' % async_jsonout_filepath)    

    
    # Create the structure for the application-compliant async response
    #
    # status:
    # utime_start:
    # utime_update:
    # app_specific_dict: {}


    my_dict = {
            'filelist' : []
            }

    async_response_dict = {
            'status' : 'RUNNING',
            'utime_start' : unixtimenow(),
            'utime_update' : unixtimenow(),
            'app_specific_dict' : my_dict
            }
    
    # Write an initial file
    logging.info('Printing initial async json output file: ' + 
                 str(async_response_dict))
    fh = open(async_jsonout_filepath, 'w')
    json.dump(async_response_dict, fh)
    fh.close()



    # Iterate through times and sleep
    curr_time = stime
    counter = 0
    while curr_time <= etime:
        
        fakefilename = 'fakefile_' + str('%02d' % counter)
        logging.debug('Processing: %s' % curr_time)
        logging.debug('   Working (sleeping) for %d seconds' % sleep_seconds)
        time.sleep(sleep_seconds)
        logging.debug('   Completed the iteration (consisting of sleep)!!!')
        # Write over async json response file
        curr_time = curr_time + datetime.timedelta(seconds=intvl_seconds)
        
        # Update the async json file
        async_response_dict['utime_update'] = unixtimenow()
        async_response_dict['app_specific_dict']['filelist'].append(fakefilename)

        # Write an updated file
        logging.info('Printing updated async json output file: ' + 
                     str(async_response_dict))
        fh = open(async_jsonout_filepath, 'w')
        json.dump(async_response_dict, fh)
        fh.close()
        
        counter += 1
        
        
        
    # All done, print final file
    async_response_dict['status'] = 'COMPLETE'
    logging.info('Printing final async json output file: ' + 
                 str(async_response_dict))
    fh = open(async_jsonout_filepath, 'w')
    json.dump(async_response_dict, fh)
    fh.close()        
        


def unixtimenow():
    import calendar
    import datetime
    return calendar.timegm(datetime.datetime.utcnow().utctimetuple())




    

def getargs():
    
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--starttime", type=str, required=True,
                        help="YYYYMMDDHHmmss")
    parser.add_argument("-e", "--endtime", type=str, required=True,
                        help="YYYYMMDDHHmmss")
    parser.add_argument("-i", "--interval", type=int, required=True,
                        help="seconds")
    parser.add_argument("-z", "--sleepintvl", type=int, required=True,
                        help="seconds - simulated work time per file timestep")
    parser.add_argument("-j", "--jsonout", type=str, required=True,
                        help="Full path to JSON async response file")
    parser.add_argument("-f", "--fakeerror",
                        help="Introduce fake error in run")
    
    try:
        args = parser.parse_args()
    except:
        logging.error('Argument parsing failed')
        raise ValueError

    return args
    
    
def str2datetime(strtime):
    
    """
    Convert strtime, in YYYYMMDDHHmmss format, to datetime object and return.
    
    If it fails, return None
    
    """

    try:
        dt = datetime.datetime.strptime(strtime, '%Y%m%d%H%M%S')
    except:
        logging.error('Failed to convert to datetime: %s' % strtime)
        dt = None
        
    return dt


if __name__=="__main__":
    main()