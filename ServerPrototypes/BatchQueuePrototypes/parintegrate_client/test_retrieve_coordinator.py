

"""
A little test piece for basic retrieval of coordinator info.  I've found
that it works with python3, but with python2 it won't because some of the
Pusher routines use subprocess.run() (which is Python 3)
"""
DISTRO_ROOT = '/home/ubuntu/ssh-push-coord'

import os
import sys
import time

sys.path.insert(0, DISTRO_ROOT)

import Pusher.Pusher as Pusher

COORDINATOR_PATH = os.path.join(DISTRO_ROOT, 'Coordinator/coordinator')

# localhost
SSH_COMMAND = '/usr/bin/ssh'
SCP_COMMAND = '/usr/bin/scp'
COORDINATOR_USER_AND_HOST = "localhost"  # often user@hostname.fqdn 
LOCAL_MSG_SCRATCH_DIR = '/tmp'    

P = Pusher.Pusher(coordinator_path=COORDINATOR_PATH,
                  ssh_command=SSH_COMMAND,
                  scp_command=SCP_COMMAND,
                  userhost=COORDINATOR_USER_AND_HOST,
                  local_msg_scratch_dir=LOCAL_MSG_SCRATCH_DIR)

success = P.retrieve_coordinator_info()


    

