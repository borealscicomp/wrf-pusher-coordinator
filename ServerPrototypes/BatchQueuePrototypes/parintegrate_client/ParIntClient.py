# -*- coding: utf-8 -*-

# This points to the distro root on the server, the Coordinator
DISTRO_ROOT = '/home/ubuntu/ssh-push-coord'

import os
import sys
import time

# I "think" this is only necessary if I run on same system as the
# Coordinator, and then I don't have to add Pusher, manually, to the
# PYTHONPATH.  I should consider getting rid of this.
sys.path.insert(0, DISTRO_ROOT)


import Pusher.Pusher as Pusher



class ParIntClient(object):
    
    def __init__(self, coordinator_path=None,
                 ssh_command=None, scp_command=None,
                 userhost=None,
                 local_msg_scratch_dir=None
                 ):
        
        self._P = Pusher.Pusher(coordinator_path=coordinator_path,
                          ssh_command=ssh_command,
                          scp_command=scp_command,
                          userhost=userhost,
                          local_msg_scratch_dir=local_msg_scratch_dir)

        self._metgrid_async_response_filepath = None
    
    
    
    def start(self, 
                           parintegrate_run_path=None,
                           parintegrate_exec_path=None,
                           numpes=None,
                           numrect=None,
                           x0=None, xn=None,
                           sleepsecs=None, 
                           iterations=None):
        

        """
        parintegrate_run_path: the driver that launches the SGE job
        parintegrate_exec_path: path to the Fortran-compiled executable
        """
        # These are used to simulate work, etc.
        SLEEP_INTVL = 1
        FAKE_ERROR = True
        
        # Form the command
        the_command = parintegrate_run_path
        the_command += ' --parintexec ' + str(parintegrate_exec_path)
        the_command += ' --pecount ' + str(numpes)
        the_command += ' --numrect ' + str(numrect)
        the_command += ' --x0 ' + str(x0)
        the_command += ' --xn ' + str(xn)
        the_command += ' --sleepsecs ' + str(sleepsecs)
        the_command += ' --iterations ' + str(iterations)
        print('the_command: ' + str(the_command))
        
        response_dict = self._P.json_request(command_str=the_command)
        
        self._metgrid_async_response_filepath = response_dict['async_response_filepath']
            
        return response_dict

    def status(self):
        
        async_response_dict = self._P.application_response(
                async_response_filepath=self._metgrid_async_response_filepath)
        
        return async_response_dict
    
if __name__ == "__main__":
    

    # As currently implemented, this won't be used unless I use the
    # COORDINATOR_USER_AND_HOST that needs it.
    WRF_CLUSTER_MASTER_PUBLIC_IP = '35.162.183.43'

    # I don't like having the client hard-code the server path to the 
    # executable, but for now I'll just have to go with it
    PARINTEXEC = DISTRO_ROOT + '/ServerPrototypes/BatchQueuePrototypes/parintegrate/parintegrate'
    PECOUNT = 4
    NUMRECT = 2000000
    X0 = 0.0
    XN = 2.0
    SLEEPSECS = 10
    ITERATIONS = 3
    
    
    
    COORDINATOR_PATH = os.path.join(DISTRO_ROOT, 'Coordinator/coordinator')
    
    PARINT_RUN_PATH = DISTRO_ROOT + '/ServerPrototypes/BatchQueuePrototypes/parintegrate/ParIntegrateRun.py'
    
    # alaskawx
    #SSH_COMMAND = '/usr/bin/ssh -i /home/morton/.AWSKeys/GenericTesting.pem'
    #SCP_COMMAND = '/usr/bin/scp -i /home/morton/.AWSKeys/GenericTesting.pem'
    
    # localhost
    SSH_COMMAND = '/usr/bin/ssh'
    SCP_COMMAND = '/usr/bin/scp'
    
    #COORDINATOR_USER_AND_HOST = "ubuntu@localhost"  # often user@hostname.fqdn 
    COORDINATOR_USER_AND_HOST = "localhost"  # often user@hostname.fqdn 
    #COORDINATOR_USER_AND_HOST = "ubuntu@" + WRF_CLUSTER_MASTER_PUBLIC_IP  # often user@hostname.fqdn 
    LOCAL_MSG_SCRATCH_DIR = '/tmp'    
    
    P = ParIntClient(coordinator_path=COORDINATOR_PATH,
                 ssh_command=SSH_COMMAND, scp_command=SCP_COMMAND,
                 userhost=COORDINATOR_USER_AND_HOST,
                 local_msg_scratch_dir=LOCAL_MSG_SCRATCH_DIR)
    
    sync_response_dict = P.start(
                           parintegrate_run_path=PARINT_RUN_PATH,
                           parintegrate_exec_path=PARINTEXEC,
                           numpes=PECOUNT,
                           numrect=NUMRECT,
                           x0=X0, xn=XN,
                           sleepsecs=SLEEPSECS, 
                           iterations=ITERATIONS)
        
    print('sync_response_dict: ' + str(sync_response_dict))        
    
    
    # Get the status and the async json response path, if available
    sync_status = sync_response_dict['status']
    async_response_filepath = sync_response_dict['async_response_filepath'] 
    
    if sync_status == 'OK':
        # Probe around for a while
        TIMEOUT_SECS = 60
        seconds = 0
        while seconds < TIMEOUT_SECS:
            async_response_dict = P.status()
            print('async_response_dict: ' + str(async_response_dict))
            if async_response_dict['status'] != 'RUNNING':
                break
            time.sleep(10)
            seconds += 10
    
    
    
    
    
    
    
    
    
    
        
        
