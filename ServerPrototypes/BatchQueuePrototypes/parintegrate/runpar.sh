#!/bin/sh

#$ -N runpar

# Make sure .e and .o files arrive in working dir
#$ -cwd

#$ -q all.q

#$ -pe mpi 4

# Merge stdout and stderr into one file
#$ -j y

###$ -m be
###$ -M you@youremailhost

/shared/WRF/LIBRARIES/mpich/bin/mpirun -np 8 ./parintegrate
