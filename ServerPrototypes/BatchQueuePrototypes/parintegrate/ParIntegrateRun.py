#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 02:17:58 2019

@author: morton
"""


import argparse
import datetime
import json
import logging
import os
import shutil
import subprocess
import sys
import time
import uuid


MPIRUN = '/shared/WRF/LIBRARIES/mpich/bin/mpirun'
logging.basicConfig(level=logging.DEBUG, filename='/tmp/ParIntegrateRun.log')



def main():
    args = getargs()
    
    logging.debug("args: " + str(args))

    parint_path = args.parintexec
    logging.debug('parint_path: ' + str(parint_path))

    # Abort if the executable is not found
    if not os.path.exists(parint_path):
        print('Executable not found: %s' % parint_path)
        raise IOError

    # Abort if executable is not executable
    if not os.access(parint_path, os.X_OK):
        print('Not executable: %s' % parint_path)
        raise IOError


    npes = int(args.pecount)
    logging.debug('npes: ' + str(npes))
    
    n = int(args.numrect)
    logging.debug('n: ' + str(n))

    x0 = float(args.x0)
    logging.debug('x0: ' + str(x0))

    xn = float(args.xn)
    logging.debug('xn: ' + str(xn))

    sleep_seconds = int(args.sleepsecs)
    logging.debug('sleep_seconds: %d' % sleep_seconds)    

    iterations = int(args.iterations)
    logging.debug('iterations: ' + str(iterations))

    async_jsonout_filepath = args.jsonout    
    logging.debug('async_jsonout_filepath: %s' % async_jsonout_filepath)    


    # Create the structure for the application-compliant async response
    #
    # status:
    # utime_start:
    # utime_update:
    # app_specific_dict: {}


    ##### NEED TO COME UP WITH A GOOD app_specific_dict
    my_dict = {
            'parout_path' : None,
            'success' : None
            }

    async_response_dict = {
            'status' : 'PENDING',
            'utime_start' : unixtimenow(),
            'utime_update' : unixtimenow(),
            'app_specific_dict' : my_dict
            }
    
    # Write an initial file
    logging.info('Printing initial async json output file: ' + 
                 str(async_response_dict))
    fh = open(async_jsonout_filepath, 'w')
    json.dump(async_response_dict, fh)
    fh.close()




    # I want to run the executable from the directory it's in, 
    # which means I need to have write permissions, write the parinput.txt
    # into it, and make sure that the paroutput.txt is written into it.

    # In real world I would check these permissions before proceeding and
    # have a good error message in the async_json response if needed, but
    # right now I'm plowing ahead and just doing it.

    # Go to the cwd for the run
    rundir = os.path.dirname(parint_path)
    os.chdir(rundir)

    # Create the parinput.txt used by parintegrate
    parinput_template = """&parint
n={numrect:<10d},
x0={x0:<8.3f},
xn={xn:<8.3f},
sleepsecs={sleepsecs:<4d},
numiterations={numiterations:<3d}
/
"""

    values = {
        'numrect' : n,
        'x0' : x0,
        'xn' : xn,
        'sleepsecs' : sleep_seconds,
        'numiterations' : iterations
        }

    try:
        fh = open('parinput.txt', 'w')
        fh.write(parinput_template.format(**values))
        fh.close()
    except:
        # Might want to have a better check and response later
        print('Write failure parinput.txt...')
        raise IOError

    # For this application, because I'm going to return its file output,
    # parout.txt, later, I want to delete it if it currently somehow exists
    if os.path.isfile('parout.txt'):
        os.remove('parout.txt')
        print('Previous parout.txt removed...')

    # Create the SGE script
    logging.debug('Creating SGE script')
    create_sge_script(path_to_script='runparint.sh',
                      path_to_mpirun=MPIRUN,
                      path_to_exec='./parintegrate',
                      job_name='runparint',
                      numpes=npes)

    # Update the status in async response
    async_response_dict['status'] = 'RUNNING'
    async_response_dict['utime_update'] = unixtimenow()
    logging.info('Updating async json output file: ' + 
                 str(async_response_dict))
    fh = open(async_jsonout_filepath, 'w')
    json.dump(async_response_dict, fh)
    fh.close()

    # Run it synchronously
    the_command = ['qsub', '-sync', 'y', 'runparint.sh']
    subprocess.call(the_command)

    # If the parout.txt exists, copy it elsewhere, then copy its path
    # into the application data and mark it success.  Otherwise, success is
    # False
    if os.path.isfile('parout.txt'):
        parout_path = '/tmp/' + str(uuid.uuid4()) + '_parout.txt'
         
        try:
            shutil.copyfile('parout.txt', parout_path) 
            async_response_dict['app_specific_dict']['parout_path'] = \
                                                              parout_path
            async_response_dict['app_specific_dict']['success'] = True
        except:
            async_response_dict['app_specific_dict']['success'] = False
    else:
        async_response_dict['app_specific_dict']['success'] = False
            
    # Prepare and write the final async json response
    async_response_dict['status'] = 'COMPLETE'
    async_response_dict['utime_update'] = unixtimenow()
    logging.info('Final async json output file: ' + 
                 str(async_response_dict))
    fh = open(async_jsonout_filepath, 'w')
    json.dump(async_response_dict, fh)
    fh.close()

    



    print ('DONE!!!')




def unixtimenow():
    import calendar
    import datetime
    return calendar.timegm(datetime.datetime.utcnow().utctimetuple())




    

def getargs():
    
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--parintexec", type=str, required=True,
                        help="parintegrate exec path")
    parser.add_argument("-c", "--pecount", type=int, required=True,
                        help="Number PEs")
    parser.add_argument("-n", "--numrect", type=int, required=True,
                        help="Number rectangles")
    parser.add_argument("-a", "--x0", type=float, required=True,
                        help="x0")
    parser.add_argument("-b", "--xn", type=float, required=True,
                        help="xn")
    parser.add_argument("-s", "--sleepsecs", type=int, required=True,
                        help="Sleep seconds")
    parser.add_argument("-i", "--iterations", type=int, required=True,
                        help="Number iterations")
    parser.add_argument("-j", "--jsonout", type=str, required=True,
                        help="Full path to JSON async response file")
    try:
        args = parser.parse_args()
    except:
        logging.error('Argument parsing failed')
        raise ValueError

    return args
    
    
def str2datetime(strtime):
    
    """
    Convert strtime, in YYYYMMDDHHmmss format, to datetime object and return.
    
    If it fails, return None
    
    """

    try:
        dt = datetime.datetime.strptime(strtime, '%Y%m%d%H%M%S')
    except:
        logging.error('Failed to convert to datetime: %s' % strtime)
        dt = None
        
    return dt



def create_sge_script(path_to_script=None,
                      path_to_mpirun=None,
                      path_to_exec=None,
                      job_name=None,
                      numpes=None):

    # Writes an SGE script to specified location

    script_fh = open(path_to_script, 'w')

    script_text = """#!/bin/sh

# Make sure .e and .o files arrive in working dir
#$ -cwd

#$ -q all.q

# Merge stdout and stderr into one file
#$ -j y
"""
    script_fh.write(script_text)

    script_text = '#$ -N %s\n' % job_name
    script_fh.write(script_text)

    script_text = '#$ -pe mpi %d\n' % numpes
    script_fh.write(script_text)

    script_text = '%s -np %d %s\n' % (path_to_mpirun, numpes, path_to_exec)
    script_fh.write(script_text)

    script_fh.close()




if __name__=="__main__":
    main()
