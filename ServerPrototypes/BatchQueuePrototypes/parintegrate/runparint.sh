#!/bin/sh

# Make sure .e and .o files arrive in working dir
#$ -cwd

#$ -q all.q

# Merge stdout and stderr into one file
#$ -j y
#$ -N runparint
#$ -pe mpi 4
/shared/WRF/LIBRARIES/mpich/bin/mpirun -np 4 ./parintegrate
