#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 02:09:03 2019

@author: morton
"""

import logging
import os
import time

import Pusher.Pusher as Pusher


LOGGING_LEVEL = logging.DEBUG



log_format_str = '%(asctime)s %(funcName)s:%(lineno)d %(levelname)s'
log_format_str += ' ---- %(message)s'
logging.basicConfig(level=LOGGING_LEVEL, 
                    format=log_format_str)


# Define the server parameters
COORDINATOR_DISTRO_ROOT = '/home/morton/git/wrf-pusher-coordinator'
WRF_SERVICES_DIR = os.path.join(COORDINATOR_DISTRO_ROOT, 'WRFServices')

PUSHER_SSH_COMMAND = '/usr/bin/ssh -p 51459'
PUSHER_SCP_COMMAND = '/usr/bin/scp -P 51459'


WRF_CLUSTER_MASTER_PUBLIC_IP = '35.162.183.43'
COORDINATOR_USER_AND_HOST = "localhost"

LOCAL_MSG_SCRATCH_DIR = '/tmp'
 #COORDINATOR_USER_AND_HOST = "ubuntu@" + WRF_CLUSTER_MASTER_PUBLIC_IP  


DEFAULT_POLLING_INTVL_SECS = 10
DEFAULT_TIMEOUT_SECS = 300




class WRFClient(object):
    
    
    
    def __init__(self,
                 coordinator_path=COORDINATOR_DISTRO_ROOT + '/Coordinator/coordinator',
                 coord_wrf_services_dir=WRF_SERVICES_DIR,
                 ssh_command=PUSHER_SSH_COMMAND,
                 scp_command=PUSHER_SCP_COMMAND,
                 userhost=COORDINATOR_USER_AND_HOST,
                 local_msg_scratch_dir=LOCAL_MSG_SCRATCH_DIR):


        self._coord_wrf_services_dir = coord_wrf_services_dir
        
        self._Pusher = Pusher.Pusher(coordinator_path=coordinator_path,
                          ssh_command=ssh_command,
                          scp_command=scp_command,
                          userhost=userhost,
                          local_msg_scratch_dir=local_msg_scratch_dir)
    

    def WRFDummySimple(self, x=0, y=0,
                       timeout_secs=DEFAULT_TIMEOUT_SECS,
                       polling_intvl_secs=DEFAULT_POLLING_INTVL_SECS):
        
        
        """
        x and y are two values that will be passed to the service
        """

        service_executable = os.path.join(self._coord_wrf_services_dir, 
                                          'WRFDummySimple.py')
        #print('SERVICE EXECUTABLE: %s' % (service_executable))

        # Parameters to tell the service how long to sleep and how
        # many iterations to go through
        sleep_secs = 5
        num_iterations = 3
        
        # Form the command that should be executed        
        logging.debug('Invoked WRFDummySimple()')
        
        # Form command for coordinator
        the_command = service_executable 
        the_command += ' --avar ' + str(x)
        the_command += ' --bvar ' + str(y)
        the_command += ' --sleepsecs ' + str(sleep_secs)
        the_command += ' --iterations ' + str(num_iterations)
        logging.debug('the_command: ' + str(the_command))
        
        sync_response_dict = self._Pusher.json_request(command_str=the_command)

        wrfdummysimple_async_response_filepath = \
                    sync_response_dict['async_response_filepath']
        
        logging.debug('async_response_filepath: %s' % wrfdummysimple_async_response_filepath)        



        # Iterate, checking status of the async stuff
        logging.debug('SYNC_RESPONSE_DICT: ' + str(sync_response_dict))
        sync_status = sync_response_dict['status']
        if sync_status == 'OK':
            seconds = 0
            while seconds < timeout_secs:
                async_response_dict = self._Pusher.application_response(
                        async_response_filepath=
                        wrfdummysimple_async_response_filepath)
                logging.debug('async_response_dict: ' + str(async_response_dict))
                async_status = async_response_dict['status']
                if async_status not in ['PENDING', 'RUNNING']:
                    break
                time.sleep(polling_intvl_secs)
                seconds += polling_intvl_secs
        else:
            logging.warning('sync_status was not OK: ' + str(sync_status))
            return None
        
        
        logging.debug('Final async_response_dict: ' + str(async_response_dict))        
        
 
    def WRFSetupRunDir(self, timeout_secs=DEFAULT_TIMEOUT_SECS,
                       polling_intvl_secs=DEFAULT_POLLING_INTVL_SECS):
        
        """
        This causes the cluster to create a WRFRun directory on the
        cluster, and returns the path assuming that what's returned
        is valid.  Otherwise we just return None
        """


        wrfrundir_path = None


        service_executable = os.path.join(self._coord_wrf_services_dir, 
                                          'WRFSetupRunDir.py')
    
        # Form the command that should be executed        
        logging.debug('Invoked WRFSetupRunDir()')
        
        # Form command for coordinator
        the_command = service_executable 
        logging.debug('the_command: ' + str(the_command))
        
        sync_response_dict = self._Pusher.json_request(command_str=the_command)

        wrfsetuprundir_async_response_filepath = \
                    sync_response_dict['async_response_filepath']
        
        logging.debug('async_response_filepath: %s' % wrfsetuprundir_async_response_filepath)             
        
        # Iterate, checking status of the async stuff
        logging.debug('SYNC_RESPONSE_DICT: ' + str(sync_response_dict))
        sync_status = sync_response_dict['status']
        if sync_status == 'OK':
            seconds = 0
            while seconds < timeout_secs:
                async_response_dict = self._Pusher.application_response(
                        async_response_filepath=
                        wrfsetuprundir_async_response_filepath)
                logging.debug('async_response_dict: ' + str(async_response_dict))
                async_status = async_response_dict['status']
                if async_status not in ['PENDING', 'RUNNING']:
                    break
                time.sleep(polling_intvl_secs)
                seconds += polling_intvl_secs
        else:
            logging.warning('sync_status was not OK: ' + str(sync_status))
            return None
        
        
        logging.debug('Final async_response_dict: ' + str(async_response_dict))        


        # Get the payload and return if all is good
        if async_response_dict['status'] == 'COMPLETE':
            wrfrundir_path = \
                async_response_dict['app_specific_dict']['wrfrundir_path']
        else:
            logging.error('Returned status was not COMPLETE')
        
        return wrfrundir_path



    def WPSNamelistCreate(self, local_namelist_path=None,
                          startdate_str=None, enddate_str=None,
                          intvl_secs=None,
                          numx_pts=None, numy_pts=None,
                          dx=None, dy=None,
                          map_proj=None,
                          proj_parms=None
                          ):



        VALID_PROJECTIONS = ['lambert']
        
        GEOG_DATA_PATH = '/shared/WRF/geog'
        
        """
        
        startdate_str and enddate_str: YYYYMMDDHH
        
        This is currently set up for a simple, 1-nest domain
        
        Also, currently, it just supports a simple Lambert-conformal-conic
        projection
        
        I am also hardcoding the geog_data_path in here, for now
        """
        
        print('hello wpsnamelistcreate()')
        


        if map_proj not in VALID_PROJECTIONS:
            print('map_proj not supported: %s' % (map_proj))
            raise ValueError


        # I'm not sure if the projection stuff I put in here is going to
        # crap out on me if I put in parameters that mean nothing...  I
        # will need to come back to this.  For now, it assumes lambert
        
        namelist_template = """&share
 wrf_core = 'ARW',
 max_dom = 1,        
 start_date = '{syear:4s}-{smonth:2s}-{sday:2s}_{shour:2s}:00:00',
 end_date = '{eyear:4s}-{emonth:2s}-{eday:2s}_{ehour:2s}:00:00',
 interval_seconds = {intvl_secs:d},
 io_form_geogrid = 2,
/

&geogrid
 parent_id = 1,
 parent_grid_ratio = 1,
 i_parent_start = 1,
 j_parent_start = 1,
 e_we = {numx:d},
 e_sn = {numy:d},
 geog_data_res = 'default',
 dx = {dx:d},
 dy = {dy:d},
 map_proj = '{map_proj:s}',
 ref_lat = {ref_lat:f},
 ref_lon = {ref_lon:f},
 truelat1 = {truelat1:f},
 truelat2 = {truelat2:f},
 stand_lon = {standlon:f},
 geog_data_path = '{geog_data_path:s}',
 /
 
 &ungrib
  out_format = 'WPS',
  prefix = 'FILE',
/
 
 &metgrid
  fg_name = 'FILE',
  io_form_metgrid = 2,
/


"""

        namelist_values = {
                "syear" : startdate_str[0:4],
                "smonth" : startdate_str[4:6],
                "sday" : startdate_str[6:8],
                "shour" : startdate_str[8:10],
                "eyear" : enddate_str[0:4],
                "emonth" : enddate_str[4:6],
                "eday" : enddate_str[6:8],
                "ehour" : enddate_str[8:10],
                "intvl_secs" : intvl_secs,
                "numx" : numx_pts,
                "numy" : numy_pts,
                "dx" : dx,
                "dy" : dy,
                "map_proj" : map_proj,
                "geog_data_path" : GEOG_DATA_PATH,
                }
       

        # Add projection info
        if map_proj == 'lambert':
            namelist_values['ref_lat'] = proj_parms['reflat']
            namelist_values['ref_lon'] = proj_parms['reflon']
            namelist_values['truelat1'] = proj_parms['truelat1']
            namelist_values['truelat2'] = proj_parms['truelat2']
            namelist_values['standlon'] = proj_parms['standlon']
        else:
            logging.error('map_proj not supported: %s' % (map_proj))
        

        try:
            fh = open(local_namelist_path, 'w')
            fh.write(namelist_template.format(**namelist_values))
            
            fh.close()
        except:
            logging.error('Write failure WPS namelist: %s' % (local_namelist_path))
            raise OSError


    def FileUpload(self, localpath=None, remotepath=None):
        
        """
        Theoretically this is simple.  Just use Pusher._scp to put
        the file where I want it
        
        Returns True or False, but the True just means an error wasn't
        detected.
        """

        success = None
        if not os.path.isfile(localpath):
            logging.error('No file localpath: %s' % (localpath))
            return False

        success = self._Pusher._scpput(
                local_path=localpath,
                remote_path=remotepath)
        
        return success


        
if __name__ == "__main__":

    """
    This is all for testing.  It's not expected that the WRFClient class
    will be used from this section, but rather a driver program.
    """
    
    WC = WRFClient() 
    #WC.WRFDummySimple(x=48, y=52)     
    map_proj = 'lambert'
    proj_dict = {
            'reflat' : 60.0,
            'reflon' : -75.0,
            'truelat1' : 30.0,
            'truelat2' : 60.0,
            'standlon' : -75.0
            }
    WC.WPSNamelistCreate(local_namelist_path='/tmp/namelisttest.wps',
                         startdate_str="2019041512", 
                         enddate_str="2019041515",
                         intvl_secs=10800,
                         numx_pts=50, numy_pts=100,
                         dx=5000, dy=5000,
                         map_proj=map_proj,
                         proj_parms=proj_dict)
        






    