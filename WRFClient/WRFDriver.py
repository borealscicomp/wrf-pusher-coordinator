#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 14 01:16:10 2019

@author: morton
"""


import argparse
import os

import WRFClient

def main():
    

    try:
        args = getargs()
    except:
        return None    
    print('args: %s' % (args))
    
    # No testing of the args here.  Hopefully problems will get caught
    # in the init of WRFClient.

    WC = WRFClient.WRFClient(
            coordinator_path=args.coordpath,
            coord_wrf_services_dir=args.wrfdir,
            ssh_command=args.ssh,
            scp_command=args.scp,
            userhost=args.userhost,
            local_msg_scratch_dir=args.localscratch)
    
    print('WC init!!!')
    #WC.WRFDummySimple(x=100, y=200, timeout_secs=60)



    print('Running WRFSetupRunDir()')
    wrfrundir_path = WC.WRFSetupRunDir(timeout_secs=20,
                                       polling_intvl_secs=5)
    print('wrfrundir_path: %s' % (wrfrundir_path))

    
    print('Running WPSNamelistCreate()')
    local_wps_namelist = '/tmp/namelisttest.wps'
    map_proj = 'lambert'
    proj_dict = {
            'reflat' : 60.0,
            'reflon' : -75.0,
            'truelat1' : 30.0,
            'truelat2' : 60.0,
            'standlon' : -75.0
            }
    WC.WPSNamelistCreate(local_namelist_path=local_wps_namelist,
                         startdate_str="2019041512", 
                         enddate_str="2019041515",
                         intvl_secs=10800,
                         numx_pts=50, numy_pts=100,
                         dx=5000, dy=5000,
                         map_proj=map_proj,
                         proj_parms=proj_dict)    
    
    print('Uploading WPS namelist')
    remote_wps_namelist = os.path.join(wrfrundir_path, 'WPS/namelist.wps')
    success = WC.FileUpload(localpath=local_wps_namelist,
                  remotepath=remote_wps_namelist)
    print('FileUpload() success: %s' % (success))










def getargs():
    

    parser = argparse.ArgumentParser()
    
    parser.add_argument("-c", "--coordpath", type=str, required=True,
                        help="Path to coordinator on server")    

    parser.add_argument("-s", "--ssh", type=str, required=True,
                        help="Local command str for ssh")

    parser.add_argument("-p", "--scp", type=str, required=True,
                        help="Local command str for scp")

    parser.add_argument("-u", "--userhost", type=str, required=True,
                        help="user@host (no user needed for localhost)")

    parser.add_argument("-m", "--localscratch", type=str, required=True,
                        help="Path to local mesg scratch dir")

    parser.add_argument("-w", "--wrfdir", type=str, required=True,
                        help="Path to WRF services dir on Coordinator")

    args = None
    try:
        args = parser.parse_args()
    except:
        print('Argument parsing failed')
        raise ValueError

    return args

if __name__=='__main__':
    
    main()
