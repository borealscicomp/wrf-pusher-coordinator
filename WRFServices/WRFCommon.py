#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 15:16:07 2019

@author: morton
"""


import json
import logging
import os

# Common definitions and functions used by all WRF applications on this server
HOME_DIR = '/home/ubuntu'
WRFRUN_ROOT_DIR = HOME_DIR  # Where WRFRun dirs will be created

WRF_ROOT_DIR = '/shared/WRF'
#MPIRUN = '/shared/WRF/LIBRARIES/mpich/bin/mpirun'
MPIRUN = os.path.join(WRF_ROOT_DIR, 'LIBRARIES/mpich/bin/mpirun')

LOGGING_ROOT_DIR = '/tmp'
LOGGING_LEVEL = logging.DEBUG

# Upper bound on PEs
MAX_PES = 64



def dict_to_json_file(the_dict=None, file_path=None):

    """
    Performs a simple write of a dict to a json file, testing for 
    success or not
    """

    success = True
    try:
        fh = open(file_path, 'w')
        json.dump(the_dict, fh)
        fh.close()
    except:
        logging.error('JSON file write error: %s' % (file_path))
        success = False

    return success

def unixtimenow():
    
    """
    Need to document what this returns - I "think" it's the seconds....
    """
    import calendar
    import datetime
    return calendar.timegm(datetime.datetime.utcnow().utctimetuple())


def str2datetime(strtime):

    """
    Convert strtime, in YYYYMMDDHHmmss format, to datetime object and return.
    
    If it fails, return None
    
    """

    try:
        dt = datetime.datetime.strptime(strtime, '%Y%m%d%H%M%S')
    except:
        logging.error('Failed to convert to datetime: %s' % strtime)
        dt = None

    return dt


def create_sge_mpirun_script(path_to_script=None,
                             path_to_mpirun=None,
                             path_to_exec=None,
                             job_name=None,
                             numpes=None):

    # Writes an SGE script to specified location

    script_fh = open(path_to_script, 'w')

    script_text = """#!/bin/sh

# Make sure .e and .o files arrive in working dir
#$ -cwd

#$ -q all.q

# Merge stdout and stderr into one file
#$ -j y
"""
    script_fh.write(script_text)

    script_text = '#$ -N %s\n' % job_name
    script_fh.write(script_text)

    script_text = '#$ -pe mpi %d\n' % numpes
    script_fh.write(script_text)

    script_text = '%s -np %d %s\n' % (path_to_mpirun, numpes, path_to_exec)
    script_fh.write(script_text)

    script_fh.close()
