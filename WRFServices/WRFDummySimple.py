#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 15:17:41 2019

@author: morton
"""



import argparse
import datetime
import json
import logging
import os
import shutil
import subprocess
import sys
import time
import uuid


import WRFCommon as WCom

logging.basicConfig(level=WCom.LOGGING_LEVEL, 
                    format='%(asctime)s %(funcName)s:%(lineno)d %(levelname)s ---- %(message)s',
                    filename=os.path.join(WCom.LOGGING_ROOT_DIR, 
                                          'WRFDummySimple.log'))
"""
Fairly simple prototype of a dummy WRF application
"""


def main():
    
    args = getargs()
    
    logging.debug("args: " + str(args))
    
    a = float(args.avar)
    b = float(args.bvar)
    
    sleep_seconds = int(args.sleepsecs)
    iterations = int(args.iterations)
    
    async_jsonout_filepath = args.jsonout
    
    # Create the structure for the application-compliant async response
    #
    # status:
    # utime_start:
    # utime_update:
    # app_specific_dict: {}


    # app_specific_dict
    my_dict = {
            'sum_of_stuff' : None,
            'success' : None
            }

    async_response_dict = {
            'status' : 'PENDING',
            'utime_start' : WCom.unixtimenow(),
            'utime_update' : WCom.unixtimenow(),
            'app_specific_dict' : my_dict
            }

    # Write an initial file
    logging.info('Printing initial async json output file: ' +
                 str(async_response_dict))
    fh = open(async_jsonout_filepath, 'w')
    json.dump(async_response_dict, fh)
    fh.close()
    

    logging.debug('   Transit from PENDING to RUNNING (sleeping for %d seconds)' % sleep_seconds)
    time.sleep(sleep_seconds)

    async_response_dict = {
            'status' : 'RUNNING',
            'utime_start' : WCom.unixtimenow(),
            'utime_update' : WCom.unixtimenow(),
            'app_specific_dict' : my_dict
            }

    # Write an initial file
    logging.info('Printing RUNNING async json output file: ' +
                 str(async_response_dict))
    fh = open(async_jsonout_filepath, 'w')
    json.dump(async_response_dict, fh)
    fh.close()

    # Iterate
    for i in range(iterations):
        
        logging.debug('Adding %d and %d and %d:' % (a, b, i)) 
        my_dict['sum_of_stuff'] = a + b + i
        logging.debug('   Working (sleeping) for %d seconds' % sleep_seconds)
        time.sleep(sleep_seconds)
        logging.debug('   Completed the iteration (consisting of sleep)!!!')

        # Update the async json file
        async_response_dict['utime_update'] = WCom.unixtimenow()
        async_response_dict['app_specific_dict'] = my_dict

        # Write an updated file
        logging.info('Printing updated async json output file: ' +
                     str(async_response_dict))
        fh = open(async_jsonout_filepath, 'w')
        json.dump(async_response_dict, fh)
        fh.close()

    # All done, print final file
    my_dict['success'] = True
    async_response_dict['app_specific_dict'] = my_dict    

    async_response_dict['status'] = 'COMPLETE'
    async_response_dict['utime_update'] = WCom.unixtimenow()
    logging.info('Printing final async json output file: ' +
                 str(async_response_dict))
    fh = open(async_jsonout_filepath, 'w')
    json.dump(async_response_dict, fh)
    fh.close()



def getargs():


    parser = argparse.ArgumentParser()

    parser.add_argument("-a", "--avar", type=float, required=True,
                        help="a")
    parser.add_argument("-b", "--bvar", type=float, required=True,
                        help="b")
    parser.add_argument("-s", "--sleepsecs", type=int, required=True,
                        help="Sleep seconds")
    parser.add_argument("-i", "--iterations", type=int, required=True,
                        help="Number iterations")
    parser.add_argument("-j", "--jsonout", type=str, required=True,
                        help="Full path to JSON async response file")
    try:
        args = parser.parse_args()
    except:
        logging.error('Argument parsing failed')
        raise ValueError

    return args




if __name__=="__main__":
    main()





