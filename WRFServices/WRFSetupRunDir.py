#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 20:09:43 2019

@author: morton
"""



import argparse
import datetime
import json
import logging
import os
import shutil
import subprocess
import sys
import time
import uuid


import WRFCommon as WCom

logging.basicConfig(level=WCom.LOGGING_LEVEL, 
                    format='%(asctime)s %(funcName)s:%(lineno)d %(levelname)s ---- %(message)s',
                    filename=os.path.join(WCom.LOGGING_ROOT_DIR, 
                                          'WRFSetupRunDir.log'))


"""
Set up a WRF Run directory asynchronously, returning the full path when
done
"""


def main():
    
    WRFSETUP = os.path.join(WCom.WRF_ROOT_DIR, 'wrfusersetup/wrfsetup.sh')
    args = getargs()
    
    logging.debug("args: " + str(args))
    
    async_jsonout_filepath = args.jsonout
    
    # Create the structure for the application-compliant async response
    #
    # status:
    # utime_start:
    # utime_update:
    # app_specific_dict: {}


    # app_specific_dict
    my_dict = {
            'wrfrundir_path' : None,
            'success' : None
            }

    async_response_dict = {
            'status' : 'PENDING',
            'utime_start' : WCom.unixtimenow(),
            'utime_update' : WCom.unixtimenow(),
            'app_specific_dict' : my_dict
            }

    # Write an initial file
    logging.info('Printing initial async json output file: ' +
                 str(async_response_dict))
    fh = open(async_jsonout_filepath, 'w')
    json.dump(async_response_dict, fh)
    fh.close()
    

    logging.debug('   Transit from PENDING to RUNNING... ')

    async_response_dict = {
            'status' : 'RUNNING',
            'utime_start' : WCom.unixtimenow(),
            'utime_update' : WCom.unixtimenow(),
            'app_specific_dict' : my_dict
            }

    # Write an initial file
    logging.debug('Printing RUNNING async json output file: ' +
                 str(async_response_dict))
    fh = open(async_jsonout_filepath, 'w')
    json.dump(async_response_dict, fh)
    fh.close()


    ###  TODO

    # Create a wrf run dir name (but don't create the dir yet)
    wrfrundir_relpath = datetime.datetime.now().strftime(
                       'WRFRun_%Y-%m-%d_%H_%M_%S')
    wrfrundir_path = os.path.join(WCom.WRFRUN_ROOT_DIR, wrfrundir_relpath)
    logging.debug('wfrundir_path: ' + str(wrfrundir_path))
    



    # Run wrfsetup
    the_command = [WRFSETUP, wrfrundir_relpath]
    logging.debug('Executing the_command: ' + str(the_command))

    # I don't know why this won't work, but the one after does...
    #result = subprocess.run(the_command,
    #               cwd=WCom.WRFRUN_ROOT_DIR)

    result=subprocess.run(the_command,  
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    cwd=WCom.WRFRUN_ROOT_DIR)
    logging.debug('result.returncode: ' + str(result.returncode))
    logging.debug('result.stdout: ' + str(result.stdout))
    logging.debug('result.stderr: ' + str(result.stderr))

    # Verify that it's done correctly - let's be kind of complete - a
    # lot depends on this being correct.  I'll run through all the 
    # checks and any False will keep it False
    setup_good = True
    if not os.path.isdir(wrfrundir_path):
        logging.error('no wrfrundir_path found: ' + str(wrfrundir_path))
        setup_good = False

    wps_dir = os.path.join(wrfrundir_path, 'WPS')
    if not os.path.isdir(wps_dir):
        logging.error('no WPS dir found: ' + str(wps_dir))
        setup_good = False

    for wpsexec in ['geogrid.exe', 'metgrid.exe', 'ungrib.exe']:
        epath = os.path.join(wps_dir, wpsexec)
        if not ( os.path.isfile(epath) and os.access(epath, os.X_OK) ):
            logging.error('Executable not good: ' + str(epath))
            setup_good = False
    

    wrf_dir = os.path.join(wrfrundir_path, 'WRFRun')
    if not os.path.isdir( wrf_dir ):
        logging.error('no WRFRun dir found: ' + str(wrf_dir) )
        setup_good = False

    for wrfexec in ['real.exe', 'wrf.exe', 'ndown.exe', 'tc.exe']:
        epath = os.path.join(wrf_dir, wrfexec)
        if not ( os.path.isfile(epath) and os.access(epath, os.X_OK) ):
            logging.error('Executable not good: ' + str(epath))
            setup_good = False
    

    if setup_good:
        # Fill in the full path of the wrfrundir to my_dict
        my_dict['wrfrundir_path'] = wrfrundir_path

        # All done, print final file
        my_dict['success'] = True
    else:
        my_dict['wrfrundir_path'] = None
        my_dict['success'] = False


    async_response_dict['app_specific_dict'] = my_dict    

    async_response_dict['status'] = 'COMPLETE'
    async_response_dict['utime_update'] = WCom.unixtimenow()
    logging.debug('Printing final async json output file: ' +
                 str(async_response_dict))
    fh = open(async_jsonout_filepath, 'w')
    json.dump(async_response_dict, fh)
    fh.close()



def getargs():


    parser = argparse.ArgumentParser()

    """
    parser.add_argument("-a", "--avar", type=float, required=True,
                        help="a")
    parser.add_argument("-b", "--bvar", type=float, required=True,
                        help="b")
    parser.add_argument("-s", "--sleepsecs", type=int, required=True,
                        help="Sleep seconds")
    parser.add_argument("-i", "--iterations", type=int, required=True,
                        help="Number iterations")
    """
    parser.add_argument("-j", "--jsonout", type=str, required=True,
                        help="Full path to JSON async response file")
    try:
        args = parser.parse_args()
    except:
        logging.error('Argument parsing failed')
        raise ValueError

    return args




if __name__=="__main__":
    main()





