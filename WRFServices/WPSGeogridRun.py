#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 20 02:59:29 2019

@author: morton
"""



import argparse
import datetime
import json
import logging
import os
import shutil
import subprocess
import sys
import time
import uuid


import WRFCommon as WCom

logging.basicConfig(level=WCom.LOGGING_LEVEL, 
                    format='%(asctime)s %(funcName)s:%(lineno)d %(levelname)s ---- %(message)s',
                    filename=os.path.join(WCom.LOGGING_ROOT_DIR, 
                                          'WPSGeogridRun.log'))
"""
Run geogrid.exe

Assumptions:
 
* namelist.wps has already been set up correctly
* 

"""


def main():
    
    args = getargs()
    
    logging.debug("args: " + str(args))
    



    """
    a = float(args.avar)
    b = float(args.bvar)
    
    sleep_seconds = int(args.sleepsecs)
    iterations = int(args.iterations)
    """
    

    async_jsonout_filepath = args.jsonout
    
    # Create the structure for the application-compliant async response
    #
    # status:
    # utime_start:
    # utime_update:
    # app_specific_dict: {}


    # app_specific_dict
    my_dict = {
            'wpsrundir' : None,
            'success' : None,
            'lastlogline' : None,
            'message' : None
            }

    async_response_dict = {
            'status' : 'PENDING',
            'utime_start' : WCom.unixtimenow(),
            'utime_update' : WCom.unixtimenow(),
            'app_specific_dict' : my_dict
            }

    # Write an initial file
    logging.info('Printing initial async json output file: ' +
                 str(async_response_dict))
    write_success = WCom.dict_to_json_file(the_dict=async_response_dict,
                                           file_path=async_jsonout_filepath)
    


    # Now, check the CLI arguments.  If there is a problem, annotate
    # in the async json output and exit
    # Check for wps dir
    wps_dir = os.path.join(args.wrfrundir, 'WPS')
    if not os.path.isdir(wps_dir):
        logging.error('WPS dir not found: %s' % (wps_dir))
        my_dict['success'] = False
        my_dict['message'] = 'Unable to find WPS dir: %s' % (wps_dir)
        async_response_dict = {
                'status' : 'COMPLETE',
                'utime_start' : WCom.unixtimenow(),
                'utime_update' : WCom.unixtimenow(),
                'app_specific_dict' : my_dict
                }
        write_success = WCom.dict_to_json_file(the_dict=async_response_dict,
                                          file_path=async_jsonout_filepath)
        return


    # Check for namelist.wps (no guarantee it's the correct one, but at
    # least we know we might be in a real WPS directory)
    namelist_wps_path = os.path.join(wps_dir, 'namelist.wps')
    if not os.path.isfile(namelist_wps_path):
        logging.error('namelist.wps not found: %s' % (namelist_wps_path))
        my_dict['success'] = False
        my_dict['message'] = 'Unable to find namelist.wps dir: %s' % (namelist_wps_path)
        async_response_dict = {
                'status' : 'COMPLETE',
                'utime_start' : WCom.unixtimenow(),
                'utime_update' : WCom.unixtimenow(),
                'app_specific_dict' : my_dict
                }
        write_success = WCom.dict_to_json_file(the_dict=async_response_dict,
                                          file_path=async_jsonout_filepath)
        return

    # Check for geogrid.exe
    ### LATER I MIGHT WANT TO CHECK THAT THIS IS EXECUTABLE
    geogrid_path = os.path.join(wps_dir, 'geogrid.exe')
    if not os.path.isfile(geogrid_path):
        logging.error('geogrid.exe not found: %s' % (geogrid_path))
        my_dict['success'] = False
        my_dict['message'] = 'Unable to find geogrid.exe: %s' % (geogrid_path)
        async_response_dict = {
                'status' : 'COMPLETE',
                'utime_start' : WCom.unixtimenow(),
                'utime_update' : WCom.unixtimenow(),
                'app_specific_dict' : my_dict
                }
        write_success = WCom.dict_to_json_file(the_dict=async_response_dict,
                                          file_path=async_jsonout_filepath)
        return

    # Check for reaonsable number of PEs - int is already checked in parsing
    num_pes = args.numpes
    if 0 < num_pes < WCom.MAX_PES:
        pass
    else:
        logging.error('num_pes out of bounds: %d, MAX_PES: %d' % 
                      (num_pes, WCom.MAX_PES))
        my_dict['success'] = False
        my_dict['message'] = 'num_pes out of bounds: %d, MAX_PES: %d' %  \
                      (num_pes, WCom.MAX_PES)
        async_response_dict = {
                'status' : 'COMPLETE',
                'utime_start' : WCom.unixtimenow(),
                'utime_update' : WCom.unixtimenow(),
                'app_specific_dict' : my_dict
                }
        write_success = WCom.dict_to_json_file(the_dict=async_response_dict,
                                          file_path=async_jsonout_filepath)
        return

        
    # Make WPS dir cwd - everything will be done relative to here
    os.chdir(wps_dir)
    
    # Create the SGE script
    WCom.create_sge_mpirun_script(path_to_script='rungeogrid.sh',
                                  path_to_mpirun=WCom.MPIRUN,
                                  path_to_exec='./geogrid.exe',
                                  job_name='geogridrun',
                                  numpes=num_pes)

    # Just for the heck of it, make sure the script exists in WPS dir
    # (Note that we gave it a relative path when creating it, so this
    # double checks)
    if not os.path.isfile( os.path.join(wps_dir, 'rungeogrid.sh') ):
        logging.error('SGE script not found: rungeogrid.sh')
        my_dict['success'] = False
        my_dict['message'] = 'Unable to find rungeogrid.sh'
        async_response_dict = {
                'status' : 'COMPLETE',
                'utime_start' : WCom.unixtimenow(),
                'utime_update' : WCom.unixtimenow(),
                'app_specific_dict' : my_dict
                }
        write_success = WCom.dict_to_json_file(the_dict=async_response_dict,
                                          file_path=async_jsonout_filepath)
        return

    # Transit to running as we get ready to run

    async_response_dict = {
            'status' : 'RUNNING',
            'utime_start' : WCom.unixtimenow(),
            'utime_update' : WCom.unixtimenow(),
            'app_specific_dict' : my_dict
            }

    # Write an initial file
    logging.info('Printing RUNNING async json output file: ' +
                 str(async_response_dict))
    fh = open(async_jsonout_filepath, 'w')
    json.dump(async_response_dict, fh)
    fh.close()

    # Launch the script, in blocking mode
    the_command = ['qsub', '-sync', 'y', 'rungeogrid.sh']

    # When done, insure good return code of 0 (this only checks qsub, not
    # the program that was being run)
    result = subprocess.run(the_command)
    return_code = result.return_code
    if return_code != 0:
        logging.error('qsub return code: %d:' % (return_code))
        my_dict['success'] = False
        my_dict['message'] = 'qsub return code: %d:' % (return_code)
        async_response_dict = {
                'status' : 'COMPLETE',
                'utime_start' : WCom.unixtimenow(),
                'utime_update' : WCom.unixtimenow(),
                'app_specific_dict' : my_dict
                }
        write_success = WCom.dict_to_json_file(the_dict=async_response_dict,
                                          file_path=async_jsonout_filepath)
        return


    # When done, check the logs and write the final async json out
    expected_log_file = 'geogrid.log.%04d' % (num_pes-1)
    
    # Check that it exists


    # Check that last line has the expected 
    #
    # *** Successful completion of program geogrid.exe **





    # All done, print final file
    my_dict['success'] = True
    async_response_dict['app_specific_dict'] = my_dict    

    async_response_dict['status'] = 'COMPLETE'
    async_response_dict['utime_update'] = WCom.unixtimenow()
    logging.info('Printing final async json output file: ' +
                 str(async_response_dict))
    fh = open(async_jsonout_filepath, 'w')
    json.dump(async_response_dict, fh)
    fh.close()



def getargs():


    parser = argparse.ArgumentParser()

    parser.add_argument("-d", "--wrfrundir", type=str, required=True,
                        help="WRF Run directory")
    parser.add_argument("-p", "--numpes", type=int, required=True,
                        help="Number of PEs")
    parser.add_argument("-j", "--jsonout", type=str, required=True,
                        help="Full path to JSON async response file")
    try:
        args = parser.parse_args()
    except:
        logging.error('Argument parsing failed')
        raise ValueError

    return args




if __name__=="__main__":
    main()





